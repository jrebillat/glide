package net.alantea.glide.test;

import static org.junit.Assert.fail;

import java.io.File;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KeyEntitiesTest
{
   private static final String DATABASEPATH = "./OpenAndCloseTestDatabase";

   private static final String KEY1 = "Key1";
   private static final String VALUE1 = "Label1";
   private static final String VALUE2 = "Label2";
   private static final String VALUE3 = "Label3";
   
   private static Glider glider;

   @Before
   public void load()
   {
      clear();
      reload();
   }
   
   public static void reload()
   {
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   public static void clear()
   {
      if (glider != null)
      {
         try
         {
            glider.clear();
            glider = null;
         }
         catch (GException e)
         {
            e.printStackTrace();
            fail(e.getMessage());
         }
      }
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
      clear();
   }

   @Test
   public void T1_SimpleCreateTest()
   {
      Entity entity = null;
      try
      {
         glider.addRegisteredKey(KEY1);
         entity = glider.createEntity(Entity.class);
         entity.setAttribute(KEY1, VALUE1);
         Assert.assertNotNull(entity);
         
         Entity searched = glider.getKeyEntity(KEY1, VALUE1);
         Assert.assertNotNull(searched);
         Assert.assertEquals(entity, searched);
         
         glider.save();
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      
      Entity searched = glider.getKeyEntity(KEY1, VALUE1);
      Assert.assertNotNull(entity);
      Assert.assertEquals(entity, searched);
   }

   @Test
   public void T2_DoubleCreateTest()
   {
      Entity entity = null;
      Entity other = null;
      try
      {
         load();
         glider.addRegisteredKey(KEY1);
         entity = glider.createEntity(Entity.class);
         entity.setAttribute(KEY1, VALUE1);
         Assert.assertNotNull(entity);

         other = glider.createEntity(Entity.class);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }

      try
      {
         other.setAttribute(KEY1, VALUE1);
         Assert.fail();
      }
      catch (GException e)
      {
      }
   }

   @Test
   public void T3_MultipleCreateTest()
   {
      Entity entity1 = null;
      Entity entity2 = null;
      Entity entity3 = null;
      try
      {
         load();
         glider.addKeyAttribute(KEY1);
         entity1 = glider.createEntity(Entity.class);
         Assert.assertNotNull(entity1);
         entity1.setAttribute(KEY1, VALUE1);

         entity2 = glider.createEntity(Entity.class);
         Assert.assertNotNull(entity2);
         entity2.setAttribute(KEY1, VALUE2);

         entity3 = glider.createEntity(Entity.class);
         Assert.assertNotNull(entity3);
         entity3.setAttribute(KEY1, VALUE3);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }

   @Test
   public void T4_MultipleManageTest()
   {
      Entity entity1 = null;
      Entity entity2 = null;
      Entity entity3 = null;
      try
      {
         load();
         glider.addKeyAttribute(KEY1);
         entity1 = glider.createEntity(Entity.class);
         Assert.assertNotNull(entity1);
         entity1.setAttribute(KEY1, VALUE1);

         entity2 = glider.createEntity(Entity.class);
         Assert.assertNotNull(entity2);
         entity2.setAttribute(KEY1, VALUE2);

         entity3 = glider.createEntity(Entity.class);
         Assert.assertNotNull(entity3);
         entity3.setAttribute(KEY1, VALUE3);
         
         Entity searched = glider.getKeyEntity(KEY1, VALUE1);
         Assert.assertNotNull(searched);
         Assert.assertEquals(entity1, searched);
         
         searched = glider.getKeyEntity(KEY1, VALUE2);
         Assert.assertNotNull(searched);
         Assert.assertEquals(entity2, searched);
         
         searched = glider.getKeyEntity(KEY1, VALUE3);
         Assert.assertNotNull(searched);
         Assert.assertEquals(entity3, searched);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
}
