package net.alantea.glide.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
   SimpleEntitiesTest.class,
   SimpleRelationsTest.class,
   AttributesTest.class,
   ComposedEntitiesTest.class,
   KeyEntitiesTest.class,
   OrderedRelationsTest.class
   })
public class AllTests
{

}
