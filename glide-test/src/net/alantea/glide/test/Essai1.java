package net.alantea.glide.test;

import java.util.List;

import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.glide.Label;

public class Essai1
{

   public static void main(String[] args)
   {
      try
      {
         Glider glider = Glider.load("C:/Temp/Essai1.xml");
         Entity entity = glider.createEntity();
         Label label = glider.createLabel("TestMe");
         entity.setLabel(label);
         List<Entity> entities = glider.getEntities();
         System.out.println("nb entities : " + entities.size());
         System.out.println("entity 1 : " + glider.getEntity(1));
         glider.save();
      }
      catch (GException e)
      {
         e.printStackTrace();
      }

   }

}
