package net.alantea.glide.test;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MultipleGlidersTest
{
   private static final String DATABASEPATH1 = "./OpenAndCloseTestDatabase1";
   private static final String DATABASEPATH2 = "./OpenAndCloseTestDatabase2";
   
   private static Glider glider1;
   private static Glider glider2;

   @Before
   public void load()
   {
      clear();
      reload();
   }
   
   public static void reload()
   {
      try
      {
         glider1 = Glider.load(DATABASEPATH1);
         glider2 = Glider.load(DATABASEPATH2);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   public static void clear()
   {
      clear(glider1, DATABASEPATH1);
      clear(glider2, DATABASEPATH2);
   }
   
   public static void clear(Glider glider, String path)
   {
      if (glider != null)
      {
         try
         {
            glider.clear();
            glider = null;
         }
         catch (GException e)
         {
            e.printStackTrace();
            fail(e.getMessage());
         }
      }
      File file = new File(path);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
      clear();
   }

   @Test
   public void T1_TwoGlidersTest()
   {
      try
      {
         Entity entity = new Entity();
         Assert.assertNotNull(entity);
         glider1.injectEntity(entity);
         glider1.save();
         glider1 = Glider.load(DATABASEPATH1);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      
      try
      {
         Entity entity1 = new Entity();
         Assert.assertNotNull(entity1);
         glider2.injectEntity(entity1);
         Entity entity2 = new Entity();
         Assert.assertNotNull(entity2);
         glider2.injectEntity(entity2);
         glider2.save();
         glider2 = Glider.load(DATABASEPATH2);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      
      List<Entity> entities1 = glider1.getEntities();
      Assert.assertNotNull(entities1);
      Assert.assertEquals(1, entities1.size());

      List<Entity> entities2 = glider2.getEntities();
      Assert.assertNotNull(entities2);
      Assert.assertEquals(2, entities2.size());
   }
}
