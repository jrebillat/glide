package net.alantea.glide.test;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.glide.Direction;
import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.glide.Relation;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SimpleRelationsTest
{
   private static final String DATABASEPATH = "./OpenAndCloseTestDatabase";

   private static final String RELATION1 = "Relation1";
   
   private static Glider glider;

   @BeforeClass
   public static void load()
   {
      clear();
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   public static void clear()
   {
      if (glider != null)
      {
         try
         {
            glider.clear();
         }
         catch (GException e)
         {
            e.printStackTrace();
            fail(e.getMessage());
         }
      }
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
      clear();
   }
   
   @Test
   public void T1_SimpleRelationCreationTest() throws GException
   {
      Entity start = glider.createEntity(Entity.class);
      Assert.assertNotNull(start);
      Entity end = glider.createEntity(Entity.class);
      Assert.assertNotNull(end);
      Relation relation = glider.createRelation(start, end, RELATION1);
      Assert.assertNotNull(relation);
      clear();
   }

   @Test
   public void T2_GettingRelationTest() throws GException
   {
      clear();
      Entity start = glider.createEntity(Entity.class);
      Assert.assertNotNull(start);
      Entity end = glider.createEntity(Entity.class);
      Assert.assertNotNull(end);
      Relation relation = glider.createRelation(start, end, RELATION1);
      Assert.assertNotNull(relation);
      
      List<Relation> relations = start.getRelations(Direction.OUTGOING, RELATION1);
      Assert.assertNotNull(relations);
      Assert.assertEquals(1, relations.size());
      
      Relation storedRelation = relations.get(0);
      Assert.assertNotNull(storedRelation);
      Assert.assertEquals(relation, storedRelation);
   }

   @Test
   public void T3_SimpleRelationSaveTest()
   {
      try
      {
         glider.save();
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   @Test
   public void T4_SimpleRelationReadingTest()
   {
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(2, entities.size());
      
      List<Relation> relations = glider.getRelations(RELATION1);
      Assert.assertNotNull(relations);
      Assert.assertEquals(1, relations.size());
   }
}
