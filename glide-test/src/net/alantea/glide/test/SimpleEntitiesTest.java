package net.alantea.glide.test;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SimpleEntitiesTest
{
   private static final String DATABASEPATH = "./OpenAndCloseTestDatabase";

   private static final String LABEL1 = "Label1";
   private static final String LABEL2 = "Label2";

   private static final int NBPACKETENTITIES = 100;
   
   private static Glider glider;

   @Before
   public void load()
   {
      clear();
      reload();
   }
   
   public static void reload()
   {
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   public static void clear()
   {
      if (glider != null)
      {
         try
         {
            glider.clear();
            glider = null;
         }
         catch (GException e)
         {
            e.printStackTrace();
            fail(e.getMessage());
         }
      }
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
      clear();
   }

   @Test
   public void T1_SimpleInjectTest()
   {
      try
      {
         Entity entity = new Entity();
         Assert.assertNotNull(entity);
         glider.injectEntity(entity);
         glider.save();
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());
   }

   @Test
   public void T2_SimpleSaveTest()
   {
      try
      {
         Entity entity = glider.createEntity(Entity.class);
         Assert.assertNotNull(entity);
         glider.save();
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());
   }
   
   @Test
   public void T3_SimpleCreationTest() throws GException
   {
      Entity entity = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity);
      
      reload();
      
      entity = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity);
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());
      
      Entity storedEntity = entities.get(0);
      Assert.assertNotNull(storedEntity);
      Assert.assertEquals(entity, storedEntity);
   }
   
   @Test
   public void T4_PacketCreationTest() throws GException
   {
      load();
      for (int i = 0; i < NBPACKETENTITIES; i++)
      {
         Entity entity = glider.createEntity(Entity.class);
         Assert.assertNotNull(entity);
      }
      try
      {
         glider.save();
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }

      reload();
      
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(NBPACKETENTITIES, entities.size());
      clear();
   }

   @Test
   public void T5_SimpleLabelTest() throws GException
   {
      load();
      Entity entity = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity);
      entity.setLabel(LABEL1);
      
      List<Entity> entities = glider.getLabeledEntities(LABEL1);
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());
      
      Entity storedEntity = entities.get(0);
      Assert.assertNotNull(storedEntity);
      Assert.assertEquals(entity, storedEntity);
      
      reload();
      Entity entity0 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity0);
      
      Entity entity1 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity1);
      entity1.setLabel(LABEL1);

      Entity entity2 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity2);
      entity2.setLabel(LABEL2);
      
      List<Entity> entities1 = glider.getLabeledEntities(LABEL1);
      Assert.assertNotNull(entities1);
      Assert.assertEquals(1, entities1.size());
      
      Entity storedEntity1 = entities1.get(0);
      Assert.assertNotNull(storedEntity1);
      Assert.assertEquals(entity1, storedEntity1);
      
      List<Entity> entities2 = glider.getLabeledEntities(LABEL2);
      Assert.assertNotNull(entities2);
      Assert.assertEquals(1, entities2.size());
      
      Entity storedEntity2 = entities2.get(0);
      Assert.assertNotNull(storedEntity2);
      Assert.assertEquals(entity2, storedEntity2);
   }

   @Test
   public void T6_MultipleLabelsTest() throws GException
   {
      load();
      Entity entity0 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity0);
      entity0.setLabel(LABEL1);
      
      Entity entity1 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity1);
      entity1.setLabel(LABEL1);
      entity1.setLabel(LABEL2);

      Entity entity2 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity2);
      entity2.setLabel(LABEL2);
      
      List<Entity> entities1 = glider.getLabeledEntities(LABEL1);
      Assert.assertNotNull(entities1);
      Assert.assertEquals(2, entities1.size());
      
      Entity storedEntity1 = entities1.get(0);
      Assert.assertNotNull(storedEntity1);
      Assert.assertTrue((entity0 == storedEntity1) || (entity1 == storedEntity1));
      
      List<Entity> entities2 = glider.getLabeledEntities(LABEL2);
      Assert.assertNotNull(entities2);
      Assert.assertEquals(2, entities2.size());
      
      Entity storedEntity2 = entities2.get(0);
      Assert.assertNotNull(storedEntity2);
      Assert.assertTrue((entity1 == storedEntity2) || (entity2 == storedEntity2));
   }
   
   @Test
   public void T7_SimpleRemoveTest() throws GException
   {
      load();
      Entity entity = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity);
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());
      
      glider.removeEntity(entity);
      List<Entity> entities1 = glider.getEntities();
      Assert.assertNotNull(entities1);
      Assert.assertEquals(0, entities1.size());
   }
   
   @Test
   public void T8_MoreRemoveTest() throws GException
   {
      load();
      Entity entity1 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity1);
      List<Entity> entities1 = glider.getEntities();
      Assert.assertNotNull(entities1);
      Assert.assertEquals(1, entities1.size());
      
      Entity entity2 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity2);
      List<Entity> entities2 = glider.getEntities();
      Assert.assertNotNull(entities2);
      Assert.assertEquals(2, entities2.size());
      
      Entity entity3 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity3);
      List<Entity> entities3 = glider.getEntities();
      Assert.assertNotNull(entities3);
      Assert.assertEquals(3, entities3.size());
      
      glider.removeEntity(entity2);
      List<Entity> entities4 = glider.getEntities();
      Assert.assertNotNull(entities4);
      Assert.assertEquals(2, entities4.size());
   }
   
   @Test
   public void T9_MGetClassesTest() throws GException
   {
      load();
      Entity entity1 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity1);
      List<Entity> entities1 = glider.getEntities();
      Assert.assertNotNull(entities1);
      Assert.assertEquals(1, entities1.size());
      
      Entity entity2 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity2);
      List<Entity> entities2 = glider.getEntities();
      Assert.assertNotNull(entities2);
      Assert.assertEquals(2, entities2.size());
      
      Entity entity3 = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity3);
      List<Entity> entities3 = glider.getEntities();
      Assert.assertNotNull(entities3);
      Assert.assertEquals(3, entities3.size());
      
      List<String> classes = glider.getClasses();
      System.out.println(classes);
   }
}
