package net.alantea.glide.test;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.glide.Direction;
import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.glide.OrderedRelation;
import net.alantea.glide.Relation;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrderedRelationsTest
{
   private static final String DATABASEPATH = "./OpenAndCloseTestDatabase";

   private static final String RELATION1 = "Relation1";
   
   private static Glider glider;

   @BeforeClass
   public static void load()
   {
      clear();
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   public static void clear()
   {
      if (glider != null)
      {
         try
         {
            glider.clear();
         }
         catch (GException e)
         {
            e.printStackTrace();
            fail(e.getMessage());
         }
      }
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
      clear();
   }
   
   @Test
   public void T1_SimpleRelationCreationTest() throws GException
   {
      Entity start = glider.createEntity(Entity.class);
      Assert.assertNotNull(start);
      Entity end1 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end1);
      OrderedRelation relation1 = glider.createOrderedRelation(start, end1, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation1);
      Assert.assertEquals((long)0, relation1.getOrder());
      
      Entity end2 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end2);
      OrderedRelation relation2 = glider.createOrderedRelation(start, end2, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation2);
      Assert.assertEquals((long)1, relation2.getOrder());
      clear();
   }

   @Test
   public void T2_RemoveRelationTest() throws GException
   {
      Entity start = glider.createEntity(Entity.class);
      Assert.assertNotNull(start);
      Entity end1 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end1);
      OrderedRelation relation1 = glider.createOrderedRelation(start, end1, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation1);
      Assert.assertEquals((long)0, relation1.getOrder());
      
      Entity end2 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end2);
      OrderedRelation relation2 = glider.createOrderedRelation(start, end2, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation2);
      Assert.assertEquals((long)1, relation2.getOrder());
      
      Entity end3 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end3);
      OrderedRelation relation3 = glider.createOrderedRelation(start, end3, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation3);
      Assert.assertEquals((long)2, relation3.getOrder());
      
      glider.removeRelation(relation2);
      Assert.assertEquals((long)1, relation3.getOrder());
      clear();
   }

   @Test
   public void T3_InsertRelationTest() throws GException
   {
      Entity start = glider.createEntity(Entity.class);
      Assert.assertNotNull(start);
      Entity end1 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end1);
      OrderedRelation relation1 = glider.createOrderedRelation(start, end1, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation1);
      Assert.assertEquals((long)0, relation1.getOrder());
      
      Entity end2 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end2);
      OrderedRelation relation2 = glider.createOrderedRelation(start, end2, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation2);
      Assert.assertEquals((long)1, relation2.getOrder());
      
      Entity end3 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end3);
      OrderedRelation relation3 = glider.createOrderedRelation(start, end3, RELATION1, 1);
      Assert.assertNotNull(relation3);
      Assert.assertEquals((long)1, relation3.getOrder());
      Assert.assertEquals((long)2, relation2.getOrder());
      clear();
   }

   @Test
   public void T4_InsertRelationAndSaveTest() throws GException
   {
      Entity start = glider.createEntity(Entity.class);
      Assert.assertNotNull(start);
      start.setAttribute("reference", "One");
      Entity end1 = glider.createEntity(Entity.class);
      start.setAttribute("reference", "Two");
      Assert.assertNotNull(end1);
      OrderedRelation relation1 = glider.createOrderedRelation(start, end1, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation1);
      Assert.assertEquals((long)0, relation1.getOrder());
      
      Entity end2 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end2);
      start.setAttribute("reference", "Three");
      OrderedRelation relation2 = glider.createOrderedRelation(start, end2, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation2);
      Assert.assertEquals((long)1, relation2.getOrder());
      
      Entity end3 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end3);
      start.setAttribute("reference", "Four");
      OrderedRelation relation3 = glider.createOrderedRelation(start, end3, RELATION1, 1);
      Assert.assertNotNull(relation3);
      Assert.assertEquals((long)1, relation3.getOrder());
      Assert.assertEquals((long)2, relation2.getOrder());
      
      glider.save();
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(4, entities.size());
      for (Entity entity : entities)
      {
         if ("One".equals(entity.getAttributeValue("reference")))
         {
            // found start entity
            List<Relation> relations = entity.getRelations(Direction.OUTGOING, RELATION1);
            Assert.assertNotNull(relations);
            Assert.assertEquals(3, relations.size());
            
            Entity nend1 = relations.get(0).getEndEntity();
            Assert.assertNotNull(nend1);
            String nend1Attr = nend1.getAttributeValue("reference");
            Assert.assertNotNull(nend1Attr);
            Assert.assertEquals("Two", nend1Attr);
            
            Entity nend2 = relations.get(1).getEndEntity();
            Assert.assertNotNull(nend2);
            String nend2Attr = nend2.getAttributeValue("reference");
            Assert.assertNotNull(nend2Attr);
            Assert.assertEquals("Four", nend2Attr);
            
            Entity nend3 = relations.get(2).getEndEntity();
            Assert.assertNotNull(nend3);
            String nend3Attr = nend3.getAttributeValue("reference");
            Assert.assertNotNull(nend2Attr);
            Assert.assertEquals("Three", nend3Attr);
         }
      }
      clear();
   }

   @Test
   public void T5_MoveRelationTest() throws GException
   {
      Entity start = glider.createEntity(Entity.class);
      Assert.assertNotNull(start);
      Entity end1 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end1);
      OrderedRelation relation1 = glider.createOrderedRelation(start, end1, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation1);
      Assert.assertEquals((long)0, relation1.getOrder());
      
      Entity end2 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end2);
      OrderedRelation relation2 = glider.createOrderedRelation(start, end2, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation2);
      Assert.assertEquals((long)1, relation2.getOrder());
      
      Entity end3 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end3);
      OrderedRelation relation3 = glider.createOrderedRelation(start, end3, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation3);
      Assert.assertEquals((long)1, relation2.getOrder());
      Assert.assertEquals((long)2, relation3.getOrder());
      
      glider.removeRelation(relation3);
      OrderedRelation relation4 = glider.createOrderedRelation(start, end3, RELATION1, 1);
      Assert.assertNotNull(relation4);
      Assert.assertEquals((long)1, relation4.getOrder());
      Assert.assertEquals((long)2, relation2.getOrder());
      clear();
   }

   @Test
   public void T6_MoveRelationAndSaveTest() throws GException
   {
      Entity start = glider.createEntity(Entity.class);
      Assert.assertNotNull(start);
      start.setAttribute("reference", "One");
      Entity end1 = glider.createEntity(Entity.class);
      start.setAttribute("reference", "Two");
      Assert.assertNotNull(end1);
      OrderedRelation relation1 = glider.createOrderedRelation(start, end1, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation1);
      Assert.assertEquals((long)0, relation1.getOrder());
      
      Entity end2 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end2);
      start.setAttribute("reference", "Three");
      OrderedRelation relation2 = glider.createOrderedRelation(start, end2, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation2);
      Assert.assertEquals((long)1, relation2.getOrder());
      
      Entity end3 = glider.createEntity(Entity.class);
      Assert.assertNotNull(end3);
      OrderedRelation relation3 = glider.createOrderedRelation(start, end3, RELATION1, Integer.MAX_VALUE);
      Assert.assertNotNull(relation3);
      Assert.assertEquals((long)1, relation2.getOrder());
      Assert.assertEquals((long)2, relation3.getOrder());
      
      glider.removeRelation(relation3);
      OrderedRelation relation4 = glider.createOrderedRelation(start, end3, RELATION1, 1);
      Assert.assertNotNull(relation4);
      Assert.assertEquals((long)1, relation4.getOrder());
      Assert.assertEquals((long)2, relation2.getOrder());
      
      glider.save();
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(4, entities.size());
      for (Entity entity : entities)
      {
         if ("One".equals(entity.getAttributeValue("reference")))
         {
            // found start entity
            List<Relation> relations = entity.getRelations(Direction.OUTGOING, RELATION1);
            Assert.assertNotNull(relations);
            Assert.assertEquals(3, relations.size());
            
            Entity nend1 = relations.get(0).getEndEntity();
            Assert.assertNotNull(nend1);
            String nend1Attr = nend1.getAttributeValue("reference");
            Assert.assertNotNull(nend1Attr);
            Assert.assertEquals("Two", nend1Attr);
            
            Entity nend2 = relations.get(1).getEndEntity();
            Assert.assertNotNull(nend2);
            String nend2Attr = nend2.getAttributeValue("reference");
            Assert.assertNotNull(nend2Attr);
            Assert.assertEquals("Four", nend2Attr);
            
            Entity nend3 = relations.get(2).getEndEntity();
            Assert.assertNotNull(nend3);
            String nend3Attr = nend3.getAttributeValue("reference");
            Assert.assertNotNull(nend2Attr);
            Assert.assertEquals("Three", nend3Attr);
         }
      }
      clear();
   }
}
