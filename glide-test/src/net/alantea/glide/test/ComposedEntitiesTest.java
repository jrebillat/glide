package net.alantea.glide.test;

import static org.junit.Assert.fail;

import java.awt.Point;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ComposedEntitiesTest
{
   private static final String DATABASEPATH = "./OpenAndCloseTestDatabase";
   
   private static Glider glider;

   @Before
   public void load()
   {
      clear();
      reload();
   }
   
   public static void reload()
   {
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   public static void clear()
   {
      if (glider != null)
      {
         try
         {
            glider.clear();
            glider = null;
         }
         catch (GException e)
         {
            e.printStackTrace();
            fail(e.getMessage());
         }
      }
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
    //  clear();
   }

   @Test
   public void T1_ClassWithStringTest()
   {
      Entity1 entity = null;
      try
      {
         entity = glider.createEntity(Entity1.class);
         Assert.assertNotNull(entity);
         entity.setContent("Hello\nYou\nAll !");
         glider.save();
      }
      catch (GException e)
      {
         e.printStackTrace();
         Assert.fail(e.getMessage());
      }
      
      reload();

      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());
      
      Entity storedEntity = entities.get(0);
      Assert.assertNotNull(storedEntity);
      Assert.assertEquals(entity.getContent(), ((Entity1) storedEntity).getContent());
   }

   @Test
   public void T2_ClassWithListTest()
   {
      Entity1 entity = null;
      String[] myList = new String[] {"One", "Two", "Three"};
      try
      {
         entity = glider.createEntity(Entity1.class);
         Assert.assertNotNull(entity);
         entity.setList(Arrays.asList(myList));
         glider.save();
      }
      catch (GException e)
      {
         e.printStackTrace();
         Assert.fail(e.getMessage());
      }

      reload();

      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());

      Entity storedEntity = entities.get(0);
      Assert.assertNotNull(storedEntity);
      List<String> theList = entity.getList();
      Assert.assertNotNull(theList);
      Assert.assertEquals(myList.length, theList.size());
      for (int i = 0; i < theList.size(); i++)
      {
         Assert.assertEquals(myList[i], theList.get(i));
      }
   }

   @SuppressWarnings("serial")
   @Test
   public void T3_ClassWithMapTest()
   {
      Entity1 entity = null;
      Map<String, String> myMap  = new HashMap<String, String>() {{
         put("key1", "value1");
         put("key2", "value2");
         put("key3", "value3");
     }};
      try
      {
         entity = glider.createEntity(Entity1.class);
         Assert.assertNotNull(entity);
         entity.setMap1(myMap);
         glider.save();
      }
      catch (GException e)
      {
         e.printStackTrace();
         Assert.fail(e.getMessage());
      }

      reload();

      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());

      Entity storedEntity = entities.get(0);
      Assert.assertNotNull(storedEntity);
      Map<String, String> theMap = entity.getMap1();
      Assert.assertNotNull(theMap);
      Assert.assertEquals(myMap.size(), theMap.keySet().size());
      for (String key : theMap.keySet())
      {
         Assert.assertTrue(myMap.keySet().contains(key));
         Assert.assertEquals(theMap.get(key), myMap.get(key));
      }
   }

   @SuppressWarnings("serial")
   @Test
   public void T4_ClassWithObjectMapTest()
   {
      Entity1 entity = null;
      Map<String, Point> myMap  = new HashMap<String, Point>() {{
         put("key1", new Point(1, 11));
         put("key2", new Point(2, 12));
         put("key3", new Point(3, 13));
     }};
      try
      {
         entity = glider.createEntity(Entity1.class);
         Assert.assertNotNull(entity);
         entity.setMap2(myMap);
         glider.save();
      }
      catch (GException e)
      {
         e.printStackTrace();
         Assert.fail(e.getMessage());
      }

      reload();

      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());

      Entity storedEntity = entities.get(0);
      Assert.assertNotNull(storedEntity);
      Map<String, Point> theMap = entity.getMap2();
      Assert.assertNotNull(theMap);
      Assert.assertEquals(3, theMap.keySet().size());
      for (String key : theMap.keySet())
      {
         Assert.assertTrue(myMap.keySet().contains(key));
         Assert.assertEquals(theMap.get(key).x, myMap.get(key).x);
         Assert.assertEquals(theMap.get(key).y, myMap.get(key).y);
      }
   }
}

class Entity1 extends Entity
{
   private String content;
   
   private List<String> list = new LinkedList<>();
   
   private Map<String, String> map1 = new HashMap<>();
   
   private Map<String, Point> map2 = new HashMap<>();

   public String getContent()
   {
      return content;
   }

   public void setContent(String content)
   {
      this.content = content;
   }

   public List<String> getList()
   {
      return list;
   }

   public void setList(List<String> content)
   {
      this.list.clear();
      this.list.addAll(content);
   }

   public Map<String, String> getMap1()
   {
      return map1;
   }

   public void setMap1(Map<String, String> content)
   {
      this.map1.clear();
      this.map1.putAll(content);
   }

   public Map<String, Point> getMap2()
   {
      return map2;
   }

   public void setMap2(Map<String, Point> content)
   {
      this.map2.clear();
      this.map2.putAll(content);
   }
}
