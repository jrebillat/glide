package net.alantea.glide.test;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AttributesTest
{
   private static final String DATABASEPATH = "./OpenAndCloseTestDatabase";

   private static final String KEY1 = "Key1";

   private static final String VALUE1 = "Value1";
   
   private static final String VALUE2 = "Value2";
   
   private static Glider glider;

   @BeforeClass
   public static void load()
   {
      clear();
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   public static void clear()
   {
      if (glider != null)
      {
         try
         {
            glider.clear();
         }
         catch (GException e)
         {
            e.printStackTrace();
            fail(e.getMessage());
         }
      }
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
      clear();
   }
   
   @Test
   public void T1_SimpleAttributeCreationTest() throws GException
   {
      Entity entity = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity);
      entity.setAttribute(KEY1, VALUE1);
      
      String value = entity.getAttributeValue(KEY1);
      Assert.assertNotNull(value);
      Assert.assertEquals(VALUE1, value);
      try
      {
         glider.save();
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }

   @Test
   public void T2_GettingSimpleAttributeTest()
   {
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());

      Entity storedEntity = entities.get(0);
      Assert.assertNotNull(storedEntity);

      String value = storedEntity.getAttributeValue(KEY1);
      Assert.assertNotNull(value);
      Assert.assertEquals(VALUE1, value);
   }
   
   @Test
   public void T3_AttributesCreationTest() throws GException
   {
      clear();
      AttributedEntity entity = glider.createEntity(AttributedEntity.class);
      Assert.assertNotNull(entity);
      entity.setIntValue(123);
      entity.setDoubleValue(123.456);
      entity.setStringValue(VALUE1);
      entity.getListValue().add(VALUE1);
      entity.getListValue().add(VALUE2);
      entity.getArrayValue()[0] = VALUE2;
      entity.getArrayValue()[1] = VALUE1;
      
      try
      {
         glider.save();
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }

   @Test
   public void T4_GettingAttributesTest()
   {
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());

      Entity storedEntity = entities.get(0);
      Assert.assertNotNull(storedEntity);
      Assert.assertTrue(storedEntity instanceof AttributedEntity);

      AttributedEntity entity = (AttributedEntity) storedEntity;
      int intValue = entity.getIntValue();
      Assert.assertEquals(123, intValue);
      double doubleValue = entity.getDoubleValue();
      Assert.assertTrue(123.456 == doubleValue);
      entity.setStringValue(VALUE1);
      String stringValue = entity.getStringValue();
      Assert.assertEquals(VALUE1, stringValue);
      
      List<String> listValue = entity.getListValue();
      Assert.assertNotNull(listValue);
      Assert.assertFalse(listValue.isEmpty());
      Assert.assertEquals(2, listValue.size());
      Assert.assertEquals(VALUE1, listValue.get(0));
      Assert.assertEquals(VALUE2, listValue.get(1));
      
      String[] arrayValue = entity.getArrayValue();
      Assert.assertNotNull(arrayValue);
      Assert.assertEquals(2, arrayValue.length);
      Assert.assertEquals(VALUE2, arrayValue[0]);
      Assert.assertEquals(VALUE1, arrayValue[1]);
   }

   @Test
   public void T5_TestedAttributeTest() throws GException
   {
      clear();
      Entity entity = glider.createEntity(Entity.class);
      Assert.assertNotNull(entity);
      
      TestedAttribute attr = new TestedAttribute(VALUE1, VALUE2);
      entity.setAttribute(KEY1, attr);
      
      TestedAttribute value = entity.getAttributeValue(KEY1);
      Assert.assertNotNull(value);
      Assert.assertEquals(attr, value);
      try
      {
         glider.save();
      }
      catch (GException e) 
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      
      try
      {
         glider = Glider.load(DATABASEPATH);
      }
      catch (GException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
      List<Entity> entities = glider.getEntities();
      Assert.assertNotNull(entities);
      Assert.assertEquals(1, entities.size());

      Entity storedEntity = entities.get(0);
      Assert.assertNotNull(storedEntity);

      TestedAttribute value1 = storedEntity.getAttributeValue(KEY1);
      Assert.assertNotNull(value1);
      Assert.assertEquals(VALUE1, value1.getValue1());
      Assert.assertEquals(VALUE2, value1.getValue2());
   }
}

class AttributedEntity extends Entity
{
   private int intValue;
   private double doubleValue;
   private String stringValue = "";
   private List<String> listValue = new LinkedList<>();
   private String[] arrayValue = new String[2];
   
   public AttributedEntity()
   {
      arrayValue[0] = "";
      arrayValue[1] = "";
   }
   
   public int getIntValue()
   {
      return intValue;
   }
   public void setIntValue(int intValue)
   {
      this.intValue = intValue;
   }
   public double getDoubleValue()
   {
      return doubleValue;
   }
   public void setDoubleValue(double doubleValue)
   {
      this.doubleValue = doubleValue;
   }
   public String getStringValue()
   {
      return stringValue;
   }
   public void setStringValue(String stringValue)
   {
      this.stringValue = stringValue;
   }
   public List<String> getListValue()
   {
      return listValue;
   }

   public String[] getArrayValue()
   {
      return arrayValue;
   }
   
}

class TestedAttribute
{
   private String value1;
   private String value2;

   TestedAttribute()
   {
   }
   
   TestedAttribute(String val1, String val2)
   {
      value1 = val1;
      value2 = val2;
   }

   public String getValue1()
   {
      return value1;
   }

   public void setValue1(String value1)
   {
      this.value1 = value1;
   }

   public String getValue2()
   {
      return value2;
   }

   public void setValue2(String value2)
   {
      this.value2 = value2;
   }
}
