package net.alantea.glide.test;

import java.util.List;

import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.glide.Label;

public class Essai2
{

   public static void main(String[] args)
   {
      try
      {
         System.out.println("---- load");
         Glider glider = Glider.load("C:/Temp/Essai2.xml");
         List<Entity> entities = glider.getEntities();
         System.out.println("nb entities : " + entities.size());
         System.out.println("---- create");
         Test2Entity entity = glider.createEntity(Test2Entity.class);
         Label label = glider.createLabel("TestMe");
         entity.setTest("I am a test");
         entity.setLabel(label);
         System.out.println("---- get");
         entities = glider.getEntities();
         System.out.println("nb entities : " + entities.size());
         System.out.println("entity 1 : " + glider.getEntity(1));
         System.out.println("---- save");
         glider.save();

         System.out.println("---- reload");
         Glider glider1 = Glider.load("C:/Temp/Essai2.xml");
         System.out.println("---- reget");
         List<Entity> entities1 = glider1.getEntities();
         System.out.println("nb entities : " + entities1.size());
         System.out.println("entity 1 : " + glider1.getEntity(1));
         System.out.println("---- end");
      }
      catch (GException e)
      {
         e.printStackTrace();
      }

   }

}

class Test2Entity extends Entity
{
   private String test;
   
   public Test2Entity() {}

   public String getTest()
   {
      return test;
   }

   public void setTest(String test)
   {
      this.test = test;
   }
   
}