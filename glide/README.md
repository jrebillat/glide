# Glide
Glide is a simple Entity-relation graph-based database based on a XML storage.

#Overview
Glide is a graph-based database. It is composed of Entities linked by Relations. Both entities and relations may have attributes and derive from the Element class. An entity may have one or more labels to filter the entities when searching.

## Disclaimer
Glide is not designed for speed, for memory usage or other ways to minimize something. It is just a way to simplify database access and object manipulation. Do not try to store more than 10000 nodes in a database or the access time will increase more than you will like.

# Database

## Creation
Creating a database consists in opening a non-existant database. This will prepare the information to be stored on the next save command.

## Open a database
A database is stored in a XML file. To open a database, use the static `Glider Glider.load(String path) throws GException` that will initiate a Glider object.

## Save a database
To store the current state of the data, use the method 
`void glider.save() throws GException`.

## Managing elements
None of the following element shall be created using a constructor. Every creation must be done through the available methods on the Glider class. See below for more explanations.

# Classes
All elements are stored in classes, sometime derived, from the set described below.

## Element
Element is the base class for all database elements (entities and relations) that should never be instantiated. It refers to a Glider (a database), has an index in the database and store a map of attributes. The available methods are :
- `long getIndex()` to get the index of an element.
- `Glider getGlider()` to get a reference on the database containing the element.
- `void setAttribute(String key, String value)` to set the value for an attribute key of the element.
- `String getAttribute(String key)` to get the value for an attribute of the element.
- `List<String> getAttributeKeys()` to get the list of attribute keys of the element.

## Relation
A relation links two entities in a direction (from start to end). It has a String type and derives from Element.The available methods are :
- `Entity getStartEntity()` to get the entity set as start of this relation.
- `Entity getEndEntity()` to get the entity set as end of this relation.
- `String getType()` to get the type of this relation.

## Label
A label is a marker on entities, with a name. The available methods are :
- `String getName()` to get the name of the label.
- `List<Entity> getEntities()` to get all the entities having this label.

## Entity
An entity is a node in the graph, with zero or more labels and potentially associated with some relations. It derives from Element. Used as it is, it may be a simple information container.
- `List<String> getLabels()` to get all the labels set on this entity.
- `boolean hasLabel(String name)` to know if the label name has been set on this entity.
- `void setLabel(String name)` sets the label name on this entity.
- `void setLabel(Label label)` sets the label on this entity.
- `void removeLabel(Label label)` removes the label from this entity.
- `List<Relation> getRelations(Direction direction)` gets the list of relations linked to this entity in the given direction (INCOMING, OIUTGOING or BOTH).
- `List<Relation> getRelations(Direction direction, String typeName)` gets the list of relations of the given type linked to this entity in the given direction (INCOMING, OIUTGOING or BOTH).

# Glider
The information from the database should be mainly accessed from the Glider created by a Glider.load() method.

## Object creation
No object should be created directly, but instead the methods from Glider should be used.
The creation methods for objects are :
- `Label glider.createLabel(String name)` to create or get a label with the given type name.
- `Entity createEntity()` to create a new raw entity.
- `<T> T createEntity(Class<T extends Entity> derivedClass)` to create a new entity of a derived type (see below).
- `Relation createRelation(Entity start, Entity end, String typeName)` creates a raw Relation between two entities, with the given type name.
- `Relation createRelation(Entity start, Entity end, String typeName, Class<T extends Relation> derivedClass)` creates a new Relation of a derived type (see below) between two entities, with the given type name.

## Working with entities
Some methods in the glider are here to manages the entities. They are :
- `List<Entity> glider.getEntities()` returns all existing entities in the database.
- `Entity glider.getEntity(long index)` returns the entity with the given index.
- `List<Entity> glider.getLabeledEntities(Label label)` returns all existing entities in the database having the label set.
- `List<Entity> glider.getLabeledEntities(String labelName)` returns all existing entities in the database having the label of this name set.
- `void glider.removeEntity(Entity entity)` removes the entity from the database.

## Working with relations
Some methods in the glider are here to manages the relations. They are :
- `List<Relation> glider.getRelations(Entity entity, Direction direction)` returns all existing relations for the given entity in the specified direction.
- `List<Relation> glider.getRelations(Entity entity, Direction direction, String typeName)` returns all existing relations of the given type for the given entity in the specified direction.
- `void glider.removeRelation(Relation relation)` removes the relation from the database.

# Example
A simple example :

     `try
      {
         System.out.println("---- load");
         Glider glider = Glider.load("C:/Temp/Essai2.xml");
         List<Entity> entities = glider.getEntities();
         System.out.println("nb entities : " + entities.size());
         System.out.println("---- create");
         Test2Entity entity = glider.createEntity(Test2Entity.class);
         Label label = glider.createLabel("TestMe");
         entity.setTest("I am a test");
         entity.setLabel(label);
         System.out.println("---- get");
         entities = glider.getEntities();
         System.out.println("nb entities : " + entities.size());
         System.out.println("entity 1 : " + glider.getEntity(1));
         System.out.println("---- save");
         glider.save();
         System.out.println("---- reload");
         Glider glider1 = Glider.load("C:/Temp/Essai2.xml");
         System.out.println("---- reget");
         List<Entity> entities1 = glider1.getEntities();
         System.out.println("nb entities : " + entities1.size());
         System.out.println("entity 1 : " + glider1.getEntity(1));
         System.out.println("---- end");
      }
      catch (GException e)
      {
         e.printStackTrace();
      }`

# Generator
A generator is available to create all the classes containing the data, with fields and accessors, from an XML file.
it may deal with :
- entities : objects that are to be stored in the base
- fields : fields in objects
- relations : connections between entities
- relation types :specific types of relations

## Using the generator
The generator is the class `net.alantea.glide.gen.Generator`. It may be called with two parameters : the XML file to use for generation and the directory into which to generate the class hierarchy.

