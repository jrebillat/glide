package net.alantea.glide.bdd;
import java.util.List;

import net.alantea.glide.Entity;
import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.utils.exception.LntException;


// TODO: Auto-generated Javadoc
/** Story class to hold configuration elements. */
public class Configuration extends Entity
{
   /** Constructor. */
   public Configuration() {}

   /**
    * Gets the configuration.
    *
    * @param glider the glider
    * @return the configuration
    */
   public static Configuration getConfiguration(Glider glider)
   {
      Configuration instance = null;
      try
      {
         List<Entity> confs = glider.getClassEntities(Configuration.class.getName());
         if ((confs != null) && (confs.size() >= 1))
         {
            instance = (Configuration) confs.get(0);
         }
         else if ((confs == null) || (confs.size() == 0))
         {
            instance = (Configuration) glider.createEntity(Configuration.class);
         }
      }
      catch (GException e)
      {
         new LntException("Error getting configuration", e);
      }
      return instance;
   }
   
    //  -------------------------- Items --------------------------
    /**
     * Gets the item.
     *
     * @param glider the glider
     * @param name the name
     * @return the attribute
     */
   public final static String getItem(Glider glider, String name)
   {
	   Configuration conf = getConfiguration(glider);
	   return conf.getAttributeValue(name);
	}

   /**
    * Sets the item.
    *
    * @param glider the glider
    * @param name the name
    * @param value the value
    */
   public final static void setItem(Glider glider, String name, Object value)
   {
      Configuration conf = getConfiguration(glider);
	   try
	   {
		   conf.setAttribute(name, value);
	   }
	   catch (GException e)
	   {
         new LntException("Error setting value '" + value + "' for '" + name + "'", e);
	   }
   }

   /**
    * Gets the item names.
    *
    * @param glider the glider
    * @return the item names
    */
   public final static List<String> getItemNames(Glider glider)
   {
      Configuration conf = getConfiguration(glider);
	   return conf.getAttributeKeys();
   }
   
   /**
    * Gets the integer item.
    *
    * @param glider the glider
    * @param name the name
    * @return the attribute
    */
  public final static int getIntegerItem(Glider glider, String name)
  {
     return getIntegerItem(glider, name, 0);
  }
  
  /**
   * Gets the integer item.
   *
   * @param glider the glider
   * @param name the name
   * @param defaultValue the default value
   * @return the attribute
   */
 public final static int getIntegerItem(Glider glider, String name, int defaultValue)
 {
    Configuration conf = getConfiguration(glider);
	  String s = conf.getAttributeValue(name);
	  if ((s != null) && (!s.isEmpty() && (s.trim().matches("^[0-9]+$") ||  s.trim().matches("^-[0-9]+$"))))
	  {
	     return Integer.parseInt(s.trim());
	  }
	  else
	  {
		  return defaultValue;
	  }
  }
  
  /**
   * Sets the item.
   *
   * @param glider the glider
   * @param name the name
   * @param value the value
   */
  public final static void setItem(Glider glider, String name, int value)
  {
     Configuration conf = getConfiguration(glider);
	  try {
		  conf.setAttribute(name, Integer.toString(value));
	  }
	  catch (GException e)
	  {
        new LntException("Error setting item value '" + value + "' for '" + name + "'", e);
	  }
  }

/**
 * Checks for item.
 *
 * @param glider the glider
 * @param name the name
 * @return true, if successful
 */
public static boolean hasItem(Glider glider, String name) {
	   return getItem(glider, name) != null;
}

}
