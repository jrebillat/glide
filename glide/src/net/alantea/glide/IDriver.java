package net.alantea.glide;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Map;

/**
 * The Interface IDriver.
 */
public interface IDriver
{

   /**
    * Read.
    *
    * @param path the path
    * @return the glider
    * @throws GException the gexception
    */
   public Glider read(String path) throws GException;

   /**
    * Write.
    *
    * @param path the path
    * @param glider the glider
    * @param keys the keys
    * @throws GException the gexception
    */
   public void write(String path, Glider glider, Map<String, String> keys) throws GException;

   /**
    * Adds a jar file.
    *
    * @param file the file
    * @throws GException the g exception
    */
   public default void addJarFile(File file) throws GException
   {
      ClassLoader systemClassLoader = IDriver.class.getClassLoader();
      if ((file.exists()) && (systemClassLoader instanceof URLClassLoader))
      {
         try
         {
            URL url = file.toURI().toURL();
            Class<?> sysclass = URLClassLoader.class;
            try
            {
               Method method = sysclass.getDeclaredMethod("addURL", new Class[] { URL.class });
               method.setAccessible(true);
               method.invoke(systemClassLoader, new Object[] { url });
            }
            catch (Throwable t)
            {
               t.printStackTrace();
               throw new GException("Error, could not add URL to system classloader", t);
            }
         }
         catch (MalformedURLException e)
         {
            throw new GException("Malformed URL", e);
         }
      }
      else
      {
         throw new GException("Adding jar files in Glider will only works with Java8, not on newer versions");
      }
   }
}
