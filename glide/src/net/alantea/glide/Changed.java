package net.alantea.glide;


@FunctionalInterface
public interface Changed
{
 /**
  * Changed.
  *
  * @param value the new value
  */
 public void  changed(Object value);
}