package net.alantea.glide;

import java.util.List;

public interface ParentElement
{
   public List<Element> getChildren() throws GException;
   
   public List<PossibleChild> getPossibleChildren();
   
   public String getChildrenLinkName(Class<? extends Element> targetClass);
   
   public default String getSimpleName() { return this.getClass().getSimpleName(); }
   
   public default String getTooltip() { return null; }
}
