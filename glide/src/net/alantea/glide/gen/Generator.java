package net.alantea.glide.gen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.alantea.glide.gen.internal.GeneratorContainer;
import net.alantea.glide.gen.internal.GeneratorElement;
import net.alantea.glide.gen.internal.GeneratorEntity;
import net.alantea.glide.gen.internal.GeneratorEnum;
import net.alantea.glide.gen.internal.GeneratorField;
import net.alantea.glide.gen.internal.GeneratorPackage;
import net.alantea.glide.gen.internal.GeneratorRelation;
import net.alantea.glide.gen.internal.GeneratorRelation.Multiple;
import net.alantea.glide.gen.internal.GeneratorRelationType;

public class Generator extends DefaultHandler
{

   /** The parser. */
   private SAXParser parser;

   /** The entities. */
   private Map<String, GeneratorEntity> generatorEntities = new HashMap<>();

   private Map<String, GeneratorEnum> generatorEnums = new HashMap<>();

   private Map<String, GeneratorRelation> generatorRelations = new HashMap<>();

   private Map<String, GeneratorRelationType> generatorRelationTypes = new HashMap<>();

   private Stack<GeneratorElement> generatorElements = new Stack<>();

   private String outPath;

   GeneratorEnum currentEnum = null;

   GeneratorEntity currentEntity = null;

   GeneratorRelationType currentRelationType = null;

   private GeneratorPackage currentPackage = null;

   private String characters = "";

   private GeneratorElement currentElement;

   private Generator(String outPath) throws SAXException
   {
      this.outPath = outPath;
      SAXParserFactory spfac = SAXParserFactory.newInstance();

      try
      {
         parser = spfac.newSAXParser();
      }
      catch (ParserConfigurationException e)
      {
         throw new SAXException(e);
      }
   }

   public static void main(String[] args)
   {
      if (args.length == 2)
      {
         try
         {
            System.out.println("Generating in " + args[0] + " from " + args[1]);
            Generator generator = new Generator(args[0]);
            generator.parser.parse(args[1], generator);
            generator.create();
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
         catch (SAXException e)
         {
            e.printStackTrace();
         }
      }
      else
      {
         System.out.println("Usage : <program> <output path> <gerenator file>");
      }
   }

   private void create() throws IOException
   {
      createEnums();
      createRelations();
      createEntities();
   }

   private void createEnums() throws IOException
   {
      for (GeneratorEnum enumerate : generatorEnums.values())
      {
         String className = setFirstUppercase(enumerate.getName());
         GeneratorPackage pack = enumerate.getPackage();
         String path = outPath + "/src/" + ((pack == null) ? "" : pack.getPackageSubDir());
         File dir = new File(path);
         dir.mkdirs();
         Writer writer = new FileWriter(path + className + ".java");
         if (pack != null)
         {
            writer.write("package " + pack.getPackage() + ";\r\n");
         }
         writer.write("/** " + enumerate.getJavadoc() + " */\r\n" + "public enum " + className + "\r\n" + "{\r\n");

         String prepend = "";
         for (String value : enumerate.getValues())
         {
            writer.write(prepend + "   " + value);
            if (prepend.isEmpty())
            {
               prepend = ",\r\n";
            }
         }
         writer.write("\r\n}\r\n");
         writer.close();
      }
   }

   private void createRelations() throws IOException
   {
      for (GeneratorRelationType generatorRelationType : generatorRelationTypes.values())
      {
         String className = setFirstUppercase(generatorRelationType.getName());
         GeneratorPackage pack = generatorRelationType.getPackage();
         String path = outPath + "/src/" + ((pack == null) ? "" : pack.getPackageSubDir());
         File dir = new File(path);
         dir.mkdirs();
         Writer writer = new FileWriter(path + className + ".java");
         if (pack != null)
         {
            writer.write("package " + pack.getPackage() + ";\r\n");
         }
         writer.write("import net.alantea.glide.Relation;\r\n" + "import net.alantea.liteprops.StringProperty;\r\n"
               + "import net.alantea.liteprops.IntegerProperty;\r\n"
               + "import net.alantea.liteprops.LongProperty;\r\n"
               + "import net.alantea.liteprops.DoubleProperty;\r\n"
               + "import net.alantea.liteprops.FloatProperty;\r\n"
               + "import net.alantea.liteprops.BooleanProperty;\r\n"
               + "import net.alantea.glide.Relation;\r\n"
               + "\r\n" + "/** " + generatorRelationType.getJavadoc() + " */\r\n" + "public class " + className
               + " extends " + generatorRelationType.getBaseClass() + "\r\n" + "{\r\n");

         // field declaration
         setFields(writer, generatorRelationType);

         writer.write("   /** Constructor. */\r\n");
         writer.write("   public " + className + "() {}\r\n\r\n");

         // field getters and setters
         setAccessors(writer, generatorRelationType);

         writer.write("}\r\n");
         writer.close();
      }
   }

   private void createEntities() throws IOException
   {

      for (GeneratorEntity generatorEntity : generatorEntities.values())
      {
         String className = setFirstUppercase(generatorEntity.getName());
         GeneratorPackage pack = generatorEntity.getPackage();
         String path = outPath + "/src/" + ((pack == null) ? "" : pack.getPackageSubDir());
         File dir = new File(path);
         dir.mkdirs();
         Writer writer = new FileWriter(path + className + ".java");
         if (pack != null)
         {
            writer.write("package " + pack.getPackage() + ";\r\n");
         }
         writer.write("import java.util.List;\r\n" + "import net.alantea.glide.GException;\r\n"
               + "import net.alantea.glide.Relation;\r\n" + "import net.alantea.glide.OrderedRelation;\r\n"
               + "import net.alantea.glide.Direction;\r\n" + "import net.alantea.glide.Entity;\r\n"
               + "import net.alantea.liteprops.StringProperty;\r\n"
               + "import net.alantea.liteprops.IntegerProperty;\r\n"
               + "import net.alantea.liteprops.LongProperty;\r\n"
               + "import net.alantea.liteprops.DoubleProperty;\r\n"
               + "import net.alantea.liteprops.FloatProperty;\r\n"
               + "import net.alantea.liteprops.BooleanProperty;\r\n"
               + "\r\n" + "\r\n" + "/** "
               + generatorEntity.getJavadoc() + " */\r\n" + "public class " + setFirstUppercase(className) + " extends "
               + generatorEntity.getBaseClass() + "\r\n" + "{\r\n");

         // field declaration
         setFields(writer, generatorEntity);

         writer.write("\r\n   /** Constructor. */\r\n");
         writer.write("   public " + className + "() {}\r\n\r\n");

         // field getters and setters
         setAccessors(writer, generatorEntity);

         // children
         for (GeneratorRelation generatorRelation : generatorRelations.values())
         {
            if ((generatorRelation.getFrom().equals(generatorEntity.getName()))
                  || (generatorRelation.getTo().equals(generatorEntity.getName())))
            {
               writer.write("\n   /** -------------------------- " + generatorRelation.getName()
                     + " -------------------------- */\n");
            }
            if (generatorRelation.getFrom().equals(generatorEntity.getName()))
            {
               if ((generatorRelation.getMultiple() == Multiple.CHILDREN)
                     || (generatorRelation.getMultiple() == Multiple.BOTH))
               {
                  writer.write("\r\n   /** " + setFirstUppercase(generatorRelation.getName())
                        + " children list property accessor. */\r\n");
                  writer.write("   public List<" + setFirstUppercase(generatorRelation.getTo()) + "> get"
                        + setFirstUppercase(generatorRelation.getName()) + setFirstUppercase(generatorRelation.getTo())
                        + "s() throws GException {" + " return getGlider().getChildren(this, \""
                        + generatorRelation.getName() + "\"); }\r\n");
                  if (generatorRelation.getOrdered())
                  {
                     writer.write("\r\n   /** Adding a " + setFirstUppercase(generatorRelation.getName())
                           + " to the list. */\r\n");
                     writer.write("   public void add" + setFirstUppercase(generatorRelation.getName())
                           + setFirstUppercase(generatorRelation.getTo()) + "( " + generatorRelation.getTo()
                           + " child) throws GException {" + " getGlider().createOrderedRelation(this, child, \""
                           + generatorRelation.getName() + "\", Integer.MAX_VALUE);}\r\n");
                     writer.write("\r\n   /** Insert a " + setFirstUppercase(generatorRelation.getName())
                           + " in the list. */\r\n");
                     writer.write("   public void insert" + setFirstUppercase(generatorRelation.getName())
                           + setFirstUppercase(generatorRelation.getTo()) + "( " + generatorRelation.getTo()
                           + " child, int position) throws GException {"
                           + " getGlider().createOrderedRelation(this, child, \"" + generatorRelation.getName()
                           + "\", position);}\r\n");
                  }
                  else
                  {
                     writer.write("\r\n   /** Adding a " + setFirstUppercase(generatorRelation.getName())
                           + " to the list. */\r\n");
                     writer.write("   public void add" + setFirstUppercase(generatorRelation.getName())
                           + setFirstUppercase(generatorRelation.getTo()) + "( " + generatorRelation.getTo()
                           + " child) throws GException {" + " getGlider().createRelation(this, child, \""
                           + generatorRelation.getName() + "\");}\r\n");
                  }
                  writer.write("\r\n   /** Removing a " + setFirstUppercase(generatorRelation.getName())
                        + " from the list. */\r\n");
                  writer.write("   public void remove" + setFirstUppercase(generatorRelation.getName())
                        + setFirstUppercase(generatorRelation.getTo()) + "( " + generatorRelation.getTo()
                        + " child) throws GException {" + " Relation relation = getGlider().getRelation(this, child,\""
                        + generatorRelation.getName() + "\");" + "getGlider().removeRelation(relation);}\r\n");
                  writer.write("\r\n   /** " + setFirstUppercase(generatorRelation.getName())
                        + " relations list property accessor. */\r\n");
                  writer.write("   public List<" + setFirstUppercase(generatorRelation.getType()) + "> get"
                        + setFirstUppercase(generatorRelation.getName()) + "s() throws GException {"
                        + " return getGlider().getRelations(this, Direction.OUTGOING, \"" + generatorRelation.getName()
                        + "\");}\r\n");
               }
               else
               {
                  writer.write(
                        "\r\n   /** " + setFirstUppercase(generatorRelation.getName()) + " property accessor. */\r\n");
                  writer.write("   public " + setFirstUppercase(generatorRelation.getTo()) + " get"
                        + setFirstUppercase(generatorRelation.getTo()) + "() throws GException {"
                        + " return getGlider().getChild(this, \"" + generatorRelation.getName() + "\"); }\r\n");
                  writer.write(
                        "\r\n   /** Setting the " + setFirstUppercase(generatorRelation.getName()) + " value. */\r\n");
                  writer.write("   public void set" + setFirstUppercase(generatorRelation.getTo()) + "( "
                        + generatorRelation.getTo() + " child) throws GException {"
                        + "   getGlider().removeRelations(this, \"" + generatorRelation.getName() + "\");"
                        + "   getGlider().createRelation(this, child, \"" + generatorRelation.getName() + "\");}\r\n");
               }
            }

            if (generatorRelation.getTo().equals(generatorEntity.getName()))
            {
               if ((generatorRelation.getMultiple() == Multiple.PARENTS)
                     || (generatorRelation.getMultiple() == Multiple.BOTH))
               {
                  writer.write("\r\n   /** " + setFirstUppercase(generatorRelation.getName())
                        + " parent list accessor. */\r\n");
                  writer.write("   public List<" + setFirstUppercase(generatorRelation.getFrom()) + "> get"
                        + setFirstUppercase(generatorRelation.getName()) + "Parents() throws GException {"
                        + " return getGlider().getParents(this, \"" + generatorRelation.getName() + "\"); }\r\n");
                  writer.write("   public List<" + setFirstUppercase(generatorRelation.getType()) + "> get"
                        + setFirstUppercase(generatorRelation.getName()) + "ParentRelations() throws GException {"
                        + " return getGlider().getRelations(this, Direction.INCOMING, \"" + generatorRelation.getName()
                        + "\");}\r\n");
               }
               else
               {
                  writer.write("   public " + setFirstUppercase(generatorRelation.getFrom()) + " get"
                        + setFirstUppercase(generatorRelation.getName()) + "Parent() throws GException {"
                        + " return getGlider().getParent(this, \"" + generatorRelation.getName() + "\"); }\r\n");
                  writer.write("   public " + setFirstUppercase(generatorRelation.getType()) + " get"
                        + setFirstUppercase(generatorRelation.getName()) + "ParentRelation() throws GException {"
                        + " return getGlider().getRelation(getGlider().getParent(this, \"" + generatorRelation.getType()
                        + "\"), this, \"" + generatorRelation.getName() + "\");}\r\n");
               }
            }
         }

         writer.write("}\r\n");
         writer.close();
      }
   }

   private void setFields(Writer writer, GeneratorContainer generatorContainer) throws IOException
   {
      for (GeneratorField generatorField : generatorContainer.getFields())
      {
         switch (generatorField.getType().toLowerCase())
         {
            case "string":
               writeSimpleField(writer, generatorField, "String", "String");
               break;
            case "integer":
               writeSimpleField(writer, generatorField, "Integer", "int");
               break;
            case "long":
               writeSimpleField(writer, generatorField, "Long", "long");
               break;
            case "float":
               writeSimpleField(writer, generatorField, "Float", "float");
               break;
            case "double":
               writeSimpleField(writer, generatorField, "Double", "double");
               break;
            case "boolean":
               writeSimpleField(writer, generatorField, "Boolean", "boolean");
               break;

            default:
               writeObjectField(writer, generatorField, generatorField.getType());
         }
      }
   }

   private void writeSimpleField(Writer writer, GeneratorField generatorField, String type, String simpleType)
         throws IOException
   {
      if (generatorField.isProperty())
      {
         writer.write("   /** " + generatorField.getJavadoc() + " */\r\n");
         writer.write("   private " + type + "Property " + setFirstLowercase(generatorField.getName()) + " = new "
               + type + "Property();\r\n");
      }
      else
      {
         writer.write("   /** " + generatorField.getJavadoc() + " */\r\n");
         writer.write("   private " + simpleType + " " + setFirstLowercase(generatorField.getName()) + ";\r\n");
      }
   }

   private void writeObjectField(Writer writer, GeneratorField generatorField, String type) throws IOException
   {
      if (generatorField.isProperty())
      {
         writer.write("   /** " + generatorField.getJavadoc() + " */\r\n");
         writer.write("   private ObjectProperty<" + type + "> = new ObjectProperty<" + type + "();\r\n");
      }
      else
      {
         writer.write("   /** " + generatorField.getJavadoc() + " */\r\n");
         writer.write("   private " + type + " " + setFirstLowercase(generatorField.getName()) + ";\r\n");
      }
   }

   private void setAccessors(Writer writer, GeneratorContainer generatorContainer) throws IOException
   {
      for (GeneratorField generatorField : generatorContainer.getFields())
      {
         switch (generatorField.getType().toLowerCase())
         {
            case "string":
               writeSimpleAccessor(writer, generatorField, "String", "StringProperty");
               break;
            case "integer":
               writeSimpleAccessor(writer, generatorField, "int", "IntegerProperty");
               break;
            case "long":
               writeSimpleAccessor(writer, generatorField, "long", "LongProperty");
               break;
            case "float":
               writeSimpleAccessor(writer, generatorField, "float", "FloatProperty");
               break;
            case "double":
               writeSimpleAccessor(writer, generatorField, "double", "DoubleProperty");
               break;
            case "boolean":
               writeSimpleAccessor(writer, generatorField, "boolean", "BooleanProperty");
               break;

            default:
               writeAccessor(writer, generatorField, generatorField.getType(), generatorField.getType());
         }
      }
      writer.write("\n");
   }

   /**
    * Write simple accessor.
    *
    * @param writer the writer
    * @param generatorField the generator field
    * @param type the type
    * @param propertyType the property type
    * @throws IOException Signals that an I/O exception has occurred.
    */
   private void writeSimpleAccessor(Writer writer, GeneratorField generatorField, String type, String propertyType)
         throws IOException
   {
      String name = generatorField.getName();
      writer.write("\n   /** -------------------------- " + name + " -------------------------- */\n");
      if (generatorField.isProperty())
      {
         writer.write("   /** Access to the " + name + " property.\n   @return the " + name + " property */\n");
         writer.write("   public final " + propertyType + " " + setFirstLowercase(name) + "Property() { return this."
               + setFirstLowercase(name) + "; }\r\n");
         writer.write("\n   /** Gets the " + name + " value.\n   @return the " + name + " value */\n");
         writer.write("   public final " + type + " get" + setFirstUppercase(name) + "() { return this."
               + setFirstLowercase(name) + ".get(); }\r\n");
         writer.write("\n   /** Sets the " + name + " value.\n   @param value the new " + name + " value */\n");
         writer.write("   public final void set" + setFirstUppercase(name) + "(" + type + " value) { this."
               + setFirstLowercase(name) + ".set(value); }\r\n");
      }
      else
      {
         writer.write("   /** Gets the " + name + " value.\n   @return the " + name + " value */\n");
         writer.write("   public final " + type + " get" + setFirstUppercase(name) + "() { return this."
               + setFirstLowercase(name) + "; }\r\n");
         writer.write("\n   /** Sets the " + name + " value.\n   @param value the new " + name + " value */\n");
         writer.write("   public final void set" + setFirstUppercase(name) + "(" + type + " value) { this."
               + setFirstLowercase(name) + " = value; }\r\n");
      }
   }

   private void writeAccessor(Writer writer, GeneratorField generatorField, String type, String propertyType)
         throws IOException
   {
      String name = generatorField.getName();
      writer.write("\n   /** -------------------------- " + name + " -------------------------- */\n");
      if (generatorField.isProperty())
      {
         writer.write("   /** Access to the " + name + " property.\n   @return the " + name + " property */\n");
         writer.write("   public final " + propertyType + " " + setFirstLowercase(name) + "Property() { return this."
               + setFirstLowercase(name) + "; }\r\n");
         writer.write("\n   /** Gets the " + name + " value.\n   @return the " + name + " value */\n");
         writer.write("   public final " + type + " get" + setFirstUppercase(name) + "() { return this."
               + setFirstLowercase(name) + ".get(); }\r\n");
         writer.write("\n   /** Sets the " + name + " value.\n   @param value the new " + name + " value */\n");
         writer.write("   public final void set" + setFirstUppercase(name) + "(" + type + " value) { this."
               + setFirstLowercase(name) + ".set(value); }\r\n");
      }
      else
      {
         writer.write("   /** Gets the " + name + " value.\n   @return the " + name + " value */\n");
         writer.write("   public final " + type + " get" + setFirstUppercase(name) + "() { return this."
               + setFirstLowercase(name) + "; }\r\n");
         writer.write("\n   /** Sets the " + name + " value.\n   @param value the new " + name + " value */\n");
         writer.write("   public final void set" + setFirstUppercase(name) + "(" + type + " value) { this."
               + setFirstLowercase(name) + " = value; }\r\n");
      }
   }

   @Override
   public final void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
   {
      if (currentElement != null)
      {
         currentElement.setJavadoc(characters);
      }
      characters = "";
      switch (qName.toLowerCase())
      {
         case "package":
            currentPackage = new GeneratorPackage();
            currentPackage.setPackage(attributes.getValue("package"));
            generatorElements.push(currentPackage);
            break;

         case "entity":
            currentEntity = new GeneratorEntity();
            currentEntity.setName(attributes.getValue("class"));
            currentEntity.setPackage(currentPackage);
            currentEntity.setBaseClass(attributes.getValue("baseClass"));
            generatorElements.push(currentEntity);
            break;

         case "enum":
            currentEnum = new GeneratorEnum();
            currentEnum.setName(attributes.getValue("class"));
            currentEnum.setPackage(currentPackage);
            generatorElements.push(currentEnum);
            break;

         case "relationtype":
            currentRelationType = new GeneratorRelationType();
            currentRelationType.setName(attributes.getValue("name"));
            currentRelationType.setPackage(currentPackage);
            currentRelationType.setBaseClass(attributes.getValue("baseClass"));
            generatorElements.push(currentRelationType);
            break;

         case "relation":
            GeneratorRelation generatorRelation = new GeneratorRelation();
            generatorRelation.setName(attributes.getValue("name"));
            generatorRelation.setType(attributes.getValue("type"));
            generatorRelation.setFrom(attributes.getValue("from"));
            generatorRelation.setTo(attributes.getValue("to"));
            generatorRelation.setOrdered(attributes.getValue("ordered"));
            String multiple = attributes.getValue("multiple");
            if (multiple != null)
            {
               switch (multiple.toUpperCase())
               {
                  case "CHILDREN":
                     generatorRelation.setMultiple(Multiple.CHILDREN);
                     break;

                  case "PARENTS":
                     generatorRelation.setMultiple(Multiple.PARENTS);
                     break;

                  case "BOTH":
                     generatorRelation.setMultiple(Multiple.BOTH);
                     break;

                  default:
                     generatorRelation.setMultiple(Multiple.NONE);
               }
            }
            generatorRelations.put(generatorRelation.getName(), generatorRelation);
            generatorElements.push(generatorRelation);
            break;

         case "field":
            GeneratorField generatorField = new GeneratorField();
            generatorField.setName(attributes.getValue("name"));
            generatorField.setType(attributes.getValue("type"));
            if (attributes.getValue("property") != null)
            {
               generatorField.setProperty(Boolean.parseBoolean(attributes.getValue("property")));
            }
            if (currentEntity != null)
            {
               currentEntity.add(generatorField);
            }
            else if (currentRelationType != null)
            {
               currentRelationType.add(generatorField);
            }
            generatorElements.push(generatorField);
            break;

         case "constant":
            currentEnum.add(attributes.getValue("value"));
            generatorElements.push(currentEnum);
            break;

         default:
            generatorElements.push(null);
      }
      currentElement = generatorElements.peek();
   }

   @Override
   public final void endElement(String uri, String localName, String qName) throws SAXException
   {
      GeneratorElement generatorElement = generatorElements.pop();
      if (generatorElement != null)
      {
         generatorElement.setJavadoc(characters);
         characters = "";
      }
      switch (qName.toLowerCase())
      {
         case "entity":
            generatorEntities.put(currentEntity.getName(), currentEntity);
            currentEntity = null;
            break;

         case "enum":
            generatorEnums.put(currentEnum.getName(), currentEnum);
            currentEnum = null;
            break;

         case "relationtype":
            generatorRelationTypes.put(currentRelationType.getName(), currentRelationType);
            currentRelationType = null;
            break;

         case "package":
            currentPackage = null;
            break;
      }
      if (!generatorElements.isEmpty())
      {
         currentElement = generatorElements.peek();
      }
      else
      {
         currentElement = null;
      }
   }

   public void characters(char ch[], int start, int length)
   {
      characters += (new String(ch, start, length));
   }

   private String setFirstUppercase(String origin)
   {
      return origin.substring(0, 1).toUpperCase() + origin.substring(1);
   }

   private String setFirstLowercase(String origin)
   {
      return origin.substring(0, 1).toLowerCase() + origin.substring(1);
   }
}
