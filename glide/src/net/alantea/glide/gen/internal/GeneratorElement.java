package net.alantea.glide.gen.internal;

public class GeneratorElement
{
   private String javadoc = "";

   public String getJavadoc()
   {
      if ((!javadoc.isEmpty()) && (!javadoc.endsWith(".")))
      {
         javadoc += ".";
      }
      return javadoc;
   }

   public void setJavadoc(String javadoc)
   {
      if ((javadoc != null) && (!javadoc.trim().isEmpty()))
      {
         this.javadoc += javadoc.trim();
      }
   }

}
