package net.alantea.glide.gen.internal;

public class GeneratorRelation extends GeneratorElement
{
   public enum Multiple
   {
      NONE,
      CHILDREN,
      PARENTS,
      BOTH
   }

   private String name;
   
   private String type = "Relation";
   
   private String from;
   
   private String to;

   private Multiple multiple;
   
   private boolean ordered;

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      if (name != null)
      {
         this.name = name;
      }
   }

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      this.type = (type == null) ? "Relation" : type;
   }

   public String getFrom()
   {
      return from;
   }

   public void setFrom(String from)
   {
      if (from != null)
      {
         this.from = from;
      }
   }

   public String getTo()
   {
      return to;
   }

   public void setTo(String to)
   {
      if (to != null)
      {
         this.to = to;
      }
   }

   public Multiple getMultiple()
   {
      return multiple;
   }

   public void setMultiple(Multiple multiple)
   {
      if (multiple != null)
      {
         this.multiple = multiple;
      }
   }

   public boolean getOrdered()
   {
      return ordered;
   }

   public void setOrdered(boolean flag)
   {
      this.ordered = flag;
   }

   public void setOrdered(String flag)
   {
      if ((flag != null) && ("TRUE".equalsIgnoreCase(flag)))
      {
         this.ordered = true;
      }
      else
      {
         this.ordered = false;
      }
   }

}
