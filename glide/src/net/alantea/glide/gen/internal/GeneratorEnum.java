package net.alantea.glide.gen.internal;

import java.util.LinkedList;
import java.util.List;

public class GeneratorEnum extends GeneratorElement
{
   private List<String> values = new LinkedList<>();
   
   private String name;
   
   private GeneratorPackage pack;

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public GeneratorPackage getPackage()
   {
      return pack;
   }

   public void setPackage(GeneratorPackage pack)
   {
      this.pack = pack;
   }
   
   public void add(String value)
   {
      if (!values.contains(value))
      {
         values.add(value);
      }
   }

   public List<String> getValues()
   {
      return values;
   }
}
