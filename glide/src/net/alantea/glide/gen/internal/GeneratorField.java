package net.alantea.glide.gen.internal;

public class GeneratorField extends GeneratorElement
{
   private String name;
   
   private String type;
   
   private boolean isProperty;

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      if (name != null)
      {
         this.name = name;
      }
   }

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      if (type != null)
      {
         this.type = type;
      }
   }

   public boolean isProperty()
   {
      return isProperty;
   }

   public void setProperty(boolean isProperty)
   {
      this.isProperty = isProperty;
   }

}
