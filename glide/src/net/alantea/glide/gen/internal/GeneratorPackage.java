package net.alantea.glide.gen.internal;

public class GeneratorPackage extends GeneratorElement
{
         
   private String pack;

   public String getPackage()
   {
      return pack;
   }
   
   public String getPackageSubDir()
   {
      return pack.replaceAll("\\.", "/") + "/";
   }

   public void setPackage(String pack)
   {
      this.pack = pack;
   }
}
