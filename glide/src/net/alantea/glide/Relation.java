package net.alantea.glide;

/**
 * The Class Relation.
 */
public class Relation extends Element
{
   
   private static final Object REASON_END = "__end__";

   private static final Object REASON_START = "__start__";

   private static final Object REASON_TYPE = "__type__";

   /** The type. */
   private String type;

   /** The start. */
   private long start;
   
   /** The end. */
   private long end;

   /**
    * Instantiates a new relation.
    */
   public Relation()
   {
   }

   /**
    * Gets the type.
    *
    * @return the type
    */
   public String getType()
   {
      return type;
   }

   /**
    * Sets the type.
    *
    * @param type the new type
    */
   void setType(String type)
   {
      this.type = type;
      fireChanged(null, REASON_TYPE);
   }

   /**
    * Gets the start entity.
    *
    * @return the start entity
    * @throws GException the g exception
    */
   public Entity getStartEntity() throws GException
   {
      return getGlider().getEntity(start);
   }

   /**
    * Gets the end entity.
    *
    * @return the end entity
    * @throws GException the g exception
    */
   public Entity getEndEntity() throws GException
   {
      return getGlider().getEntity(end);
   }

   /**
    * Gets the start.
    *
    * @return the start
    */
   long getStart()
   {
      return start;
   }

   /**
    * Sets the start.
    *
    * @param index the new start
    */
   void setStart(long index)
   {
      start = index;
      fireChanged(null, REASON_START);
   }

   /**
    * Gets the end.
    *
    * @return the end
    */
   long getEnd()
   {
      return end;
   }

   /**
    * Sets the end.
    *
    * @param index the new end
    */
   void setEnd(long index)
   {
      end = index;
      fireChanged(null, REASON_END);
   }
}
