package net.alantea.glide;

/**
 * The Interface FunctionalComparison.
 */
@FunctionalInterface
public interface FunctionalComparison
{
   
   /**
    * Compare.
    *
    * @param o1 the first object
    * @param o2 the second object
    * @return the comparison result
    * @throws GException the database exception
    */
   public int compare(Object o1, Object o2) throws GException;

}
