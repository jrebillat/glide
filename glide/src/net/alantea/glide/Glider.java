package net.alantea.glide;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.alantea.glide.storex.StorexDriver;

/**
 * The Class Glider.
 */
public class Glider
{

   /** The driver. */
   private static IDriver driver = null;

   /** The path. */
   private transient String path;

   /** The classes map. */
   private transient Map<String, List<Entity>> classesMap = new HashMap<>();

   /** The entities map. */
   private transient Map<Long, Entity> entitiesMap = new HashMap<>();

   /** The labels map. */
   private transient Map<String, Label> labelsMap = new HashMap<>();

   /** The key elements map. */
   private transient Map<String, Map<String, Element>> keyElementsMap = new HashMap<>();

   /** The key attributes. */
   private List<String> keyAttributes = new LinkedList<>();

   /** The next index. */
   private long nextIndex = 1;

   /** The entities. */
   private List<Entity> entities = new LinkedList<>();

   /** The labels. */
   private List<Label> labels = new LinkedList<>();

   /** The relations. */
   private List<Relation> relations = new LinkedList<>();
   
   private transient Map<String, Object> information = new HashMap<>();

   /**
    * Instantiates a new glider.
    */
   public Glider()
   {
   }

   // ------------------------------------------------------------------------------
   // Storage management

   /**
    * Sets the driver.
    *
    * @param newDriver the new driver
    */
   public static void setDriver(IDriver newDriver)
   {
      driver = newDriver;
   }

   /**
    * Load.
    *
    * @param path the path
    * @return the glider
    * @throws GException the g exception
    */
   public static Glider load(String path) throws GException
   {
      return load(path, null, null);
   }

   /**
    * Load.
    *
    * @param path the path
    * @param map the replacements map
    * @return the glider
    * @throws GException the g exception
    */
   public static Glider load(String path, Map<String, String> map, GliderRunnable runnable) throws GException
   {
      Glider glider;
      initializeDriver();
      if (new File(path).exists())
      {
         glider = driver.read(path);
         for (Entity entity : glider.entities)
         {
            entity.setGlider(glider);
            glider.entitiesMap.put(entity.getIndex(), entity);
            List<Entity> entitisList = glider.classesMap.get(entity.getClass().getName());
            if (entitisList == null)
            {
               entitisList = new LinkedList<>();
               glider.classesMap.put(entity.getClass().getName(), entitisList);
            }
            entitisList.add(entity);
            glider.storeKeyElements(entity);
         }

         for (Label label : glider.labels)
         {
            glider.labelsMap.put(label.getName(), label);
         }

         for (Relation relation : glider.relations)
         {
            relation.setGlider(glider);
            relation.getStartEntity().addOutgoingRelation(relation);
            relation.getEndEntity().addIncomingRelation(relation);
            glider.storeKeyElements(relation);
         }
      }
      else
      {
         glider = new Glider();
         if ((runnable != null) && (glider != null))
         {
            runnable.execute(glider);
         }
      }
      glider.path = path;
      return glider;
   }
   
   public static void initializeDriver()
   {
      if (driver == null)
      {
         driver = new StorexDriver();
      }
   }

   /**
    * Adds the jar file.
    *
    * @param file the file
    * @throws GException when raised
    */
   public static void addJarFile(File file) throws GException
   {
      initializeDriver();
      driver.addJarFile(file);
   }

   /**
    * Clear.
    *
    * @throws GException the g exception
    */
   public void clear() throws GException
   {
      classesMap.clear();
      entitiesMap.clear();
      labelsMap.clear();
      entities.clear();
      labels.clear();
      relations.clear();
      keyAttributes.clear();
      keyElementsMap.clear();
      nextIndex = 1;
   }
   
//   /**
//    * Sets the key value.
//    *
//    * @param key the key
//    * @param value the value
//    */
//   public void setKeyValue(String key, String value)
//   {
//      keyValuesMap.put(key, value);
//   }
//   
//   /**
//    * Gets the key value.
//    *
//    * @param key the key
//    * @return the key value
//    */
//   public String getKeyValue(String key)
//   {
//      return keyValuesMap.get(key);
//   }

   /**
    * Save.
    *
    * @throws GException the g exception
    */
   public void save() throws GException
   {
      initializeDriver();
      driver.write(path, this, null);
   }

   // ------------------------------------------------------------------------------
   // Entity management

   /**
    * Gets the entities.
    *
    * @return the entities
    */
   public List<Entity> getEntities()
   {
      return Collections.unmodifiableList(entities);
   }

   /**
    * Creates the entity.
    *
    * @return the entity
    * @throws GException the g exception
    */
   public Entity createEntity() throws GException
   {
      return createEntity(Entity.class);
   }

   /**
    * Creates the entity from Class.
    *
    * @param <T> the generic type
    * @param cl the cl
    * @return the entity
    * @throws GException the g exception
    */
   public <T extends Entity> T createEntity(Class<T> cl) throws GException
   {
      T entity = null;
      try
      {
         Constructor<T> constructor = cl.getDeclaredConstructor();
         constructor.setAccessible(true);
         entity = constructor.newInstance();
      }
      catch (SecurityException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException
            | InstantiationException | IllegalAccessException e)
      {
         e.printStackTrace();
      }
      injectEntity(entity);
      entity.onCreationDone();
      return entity;
   }

   /**
    * Inject the entity in the Glider.
    *
    * @param entity the entity
    * @throws GException the g exception
    */
   public void injectEntity(Entity entity) throws GException
   {
      if (entity.getGlider() != null)
      {
         throw new GException();
      }
      entity.setGlider(this);
      entity.setIndex(nextIndex);
      entitiesMap.put(nextIndex, entity);
      entities.add(entity);
      nextIndex++;

      Class<?> cl = entity.getClass();
      List<Entity> list = classesMap.get(cl.getName());
      if (list == null)
      {
         list = new LinkedList<>();
         classesMap.put(cl.getName(), list);
      }
      list.add(entity);
      storeKeyElements(entity);
   }

   /**
    * Removes the entity.
    *
    * @param entity the entity
    * @throws GException the g exception
    */
   public void removeEntity(Entity entity) throws GException
   {
      if (entity == null)
      {
         return;
      }
      if (!entities.contains(entity))
      {
         throw new GException(new NullPointerException());
      }
      removeRelations(entity);
      entities.remove(entity);
      entitiesMap.remove(entity.getIndex());
      List<Entity> entitiesList = classesMap.get(entity.getClass().getName());
      entitiesList.remove(entity);
      removeFromKeyElementsMap(entity);
   }

   /**
    * Gets the entity.
    *
    * @param index the index
    * @return the entity
    * @throws GException the g exception
    */
   public Entity getEntity(long index) throws GException
   {
      Entity entity = entitiesMap.get(index);
      if (entity == null)
      {
         throw new GException(new NullPointerException());
      }
      return entity;
   }
   
   /**
    * Gets the classes.
    *
    * @return the classes
    */
   public List<String> getClasses()
   {
      return new LinkedList<String>(classesMap.keySet());
   }

   // ------------------------------------------------------------------------------
   // Relation management

   /**
    * Creates the relation.
    *
    * @param start the start
    * @param end the end
    * @param type the type
    * @return the relation
    * @throws GException the g exception
    */
   public Relation createRelation(Entity start, Entity end, String type) throws GException
   {
      return createRelation(start, end, type, Relation.class);
   }

   /**
    * Creates the relation.
    *
    * @param start the start
    * @param end the end
    * @param type the type
    * @param targetIndex the target index
    * @return the relation
    * @throws GException the g exception
    */
   public OrderedRelation createOrderedRelation(Entity start, Entity end, String type, long targetIndex)
         throws GException
   {
      OrderedRelation relation = createRelation(start, end, type, OrderedRelation.class);
      relation.setOrder(targetIndex);
      return relation;
   }

   /**
    * Creates the relation from a class.
    *
    * @param <T> the generic type
    * @param start the start
    * @param end the end
    * @param type the type
    * @param cl the cl
    * @return the relation
    * @throws GException the g exception
    */
   public <T extends Relation> T createRelation(Entity start, Entity end, String type, Class<T> cl) throws GException
   {
      T relation = null;
      try
      {
         try
         {
            cl.getConstructor().setAccessible(true);
         }
         catch (SecurityException | NoSuchMethodException e)
         {
            e.printStackTrace();
         }
         relation = cl.getDeclaredConstructor().newInstance();
         relation.setGlider(this);
         relation.setIndex(nextIndex);
         relation.setStart(start.getIndex());
         relation.setEnd(end.getIndex());
         relation.setType(type);
         relations.add(relation);
         start.addOutgoingRelation(relation);
         end.addIncomingRelation(relation);
         storeKeyElements(relation);
         nextIndex++;
      }
      catch (InstantiationException | IllegalAccessException | IllegalArgumentException
            | InvocationTargetException | NoSuchMethodException | SecurityException e)
      {
         e.printStackTrace();
      }
      return relation;
   }

   /**
    * Removes the relation.
    *
    * @param relation the relation
    * @throws GException the g exception
    */
   public void removeRelation(Relation relation) throws GException
   {
      if (!relations.contains(relation))
      {
         throw new GException(new NullPointerException());
      }
      if (relation instanceof OrderedRelation)
      {
         // set relation at end of list
         ((OrderedRelation) relation).setOrder(Integer.MAX_VALUE);
      }
      relation.getStartEntity().removeOutgoingRelation(relation);
      relation.getEndEntity().removeIncomingRelation(relation);
      relations.remove(relation);
      removeFromKeyElementsMap(relation);
   }

   /**
    * Removes the relations.
    *
    * @param entity the entity
    * @param type the type
    * @throws GException the g exception
    */
   public void removeRelations(Entity entity, String type) throws GException
   {
      for (Relation relation : getRelations(entity, Direction.BOTH))
      {
         if (relation.getType().equals(type))
         {
            removeRelation(relation);
         }
      }
   }

   /**
    * Removes the relations.
    *
    * @param entity the entity
    * @throws GException the g exception
    */
   public void removeRelations(Entity entity) throws GException
   {
      for (Relation relation : getRelations(entity, Direction.BOTH))
      {
         removeRelation(relation);
      }
   }

   /**
    * Gets the relations.
    *
    * @param <T> the generic type
    * @param entity the entity
    * @param direction the direction
    * @return the relations
    */
   @SuppressWarnings("unchecked")
   public <T extends Relation> List<T> getRelations(Entity entity, Direction direction)
   {
      return (List<T>) entity.getRelations(direction);
   }

   /**
    * Gets the relation if there is one and only one.
    *
    * @param <T> the generic type
    * @param start the start
    * @param end the end
    * @param type the type
    * @return the relations
    */
   @SuppressWarnings("unchecked")
   public <T extends Relation> T getRelation(Entity start, Entity end, String type)
   {
      List<Relation> relations = getRelations(start, Direction.OUTGOING, type);
      if (relations != null)
      {
         for (Relation relation : relations)
         {
            if (relation.getEnd() == end.getIndex())
            {
               return (T) relation;
            }
         }
      }
      return null;
   }

   /**
    * Gets the relations.
    *
    * @param <T> the generic type
    * @param entity the entity
    * @param direction the direction
    * @param type the type
    * @return the relations
    */
   @SuppressWarnings("unchecked")
   public <T extends Relation> List<T> getRelations(Entity entity, Direction direction, String type)
   {
      return (List<T>) entity.getRelations(direction, type);
   }

   /**
    * Gets the relations of a type.
    *
    * @param <T> the generic type
    * @param type the type
    * @return the relations
    */
   @SuppressWarnings("unchecked")
   public <T extends Relation> List<T> getRelations(String type)
   {
      List<T> ret = new LinkedList<>();
      for (Relation relation : relations)
      {
         if (relation.getType().equals(type))
         {
            ret.add((T) relation);
         }
      }
      return ret;
   }

   /**
    * Gets the relations.
    *
    * @param <T> the generic type
    * @param entity the entity
    * @param direction the direction
    * @param type the type
    * @param key the key
    * @param value the value
    * @return the relations
    */
   @SuppressWarnings("unchecked")
   public <T extends Relation> List<T> getRelationsWhere(Entity entity, Direction direction, String type, String key,
         String value)
   {
      List<T> ret = new LinkedList<>();
      long index = entity.getIndex();
      for (Relation relation : entity.getRelations(direction, type))
      {
         if (relation.getType().equals(type))
         {
            boolean analyze = false;
            if ((direction == Direction.INCOMING) || (direction == Direction.BOTH))
            {
               if (relation.getEnd() == index)
               {
                  analyze = true;
               }
            }
            if ((direction == Direction.OUTGOING) || (direction == Direction.BOTH))
            {
               if (relation.getStart() == index)
               {
                  analyze = true;
               }
            }
            if (analyze)
            {
               if (relation.hasValue(key, value))
               {
                  ret.add((T) relation);
               }
            }
         }
      }
      return ret;
   }

   // ------------------------------------------------------------------------------
   // Label management

   /**
    * Creates the label.
    *
    * @param name the name
    * @return the label
    */
   public Label createLabel(String name)
   {
      Label label = labelsMap.get(name);
      if (label == null)
      {
         label = new Label(name);
         labelsMap.put(name, label);
         labels.add(label);
      }
      return label;
   }

   /**
    * Gets the labeled entities.
    *
    * @param label the label
    * @return the labeled entities
    */
   public List<Entity> getLabeledEntities(Label label)
   {
      return getLabeledEntities(label.getName());
   }

   /**
    * Gets the labeled entities.
    *
    * @param label the label
    * @return the labeled entities
    */
   public List<Entity> getLabeledEntities(String label)
   {
      List<Entity> ret = new LinkedList<>();
      for (Entity entity : entities)
      {
         if (entity.hasLabel(label))
         {
            ret.add(entity);
         }
      }
      return ret;
   }
   // ------------------------------------------------------------------------------
   // Complex management

   /**
    * Gets the related entities.
    *
    * @param <T> the generic type
    * @param entity the entity
    * @param direction the direction
    * @param type the type
    * @return the related entities
    * @throws GException the g exception
    */
   @SuppressWarnings("unchecked")
   public <T extends Entity> List<T> getRelatedEntities(Entity entity, Direction direction, String type)
         throws GException
   {
      List<T> ret = new LinkedList<>();
      if ((direction == Direction.INCOMING) || (direction == Direction.BOTH))
      {
         for (Relation relation : getRelations(entity, direction, type))
         {
            ret.add((T) relation.getStartEntity());
         }
      }
      if ((direction == Direction.OUTGOING) || (direction == Direction.BOTH))
      {
         for (Relation relation : getRelations(entity, direction, type))
         {
            ret.add((T) relation.getEndEntity());
         }
      }
      return ret;
   }

   /**
    * Gets the children.
    *
    * @param <T> the generic type
    * @param entity the entity
    * @param type the type
    * @return the children
    * @throws GException the g exception
    */
   @SuppressWarnings("unchecked")
   public <T extends Entity> List<T> getChildren(Entity entity, String type) throws GException
   {
      List<T> ret = new LinkedList<>();
      for (Relation relation : getRelations(entity, Direction.OUTGOING, type))
      {
         ret.add((T) relation.getEndEntity());
      }
      return ret;
   }

   /**
    * Gets the child.
    *
    * @param <T> the generic type
    * @param entity the entity
    * @param type the type
    * @return the child
    * @throws GException the g exception
    */
   @SuppressWarnings("unchecked")
   public <T extends Entity> T getChild(Entity entity, String type) throws GException
   {
      for (Relation relation : getRelations(entity, Direction.OUTGOING, type))
      {
         return (T) relation.getEndEntity();
      }
      return null;
   }

   /**
    * Gets the parents.
    *
    * @param <T> the generic type
    * @param entity the entity
    * @param type the type
    * @return the parents
    * @throws GException the g exception
    */
   @SuppressWarnings("unchecked")
   public <T extends Entity> List<T> getParents(Entity entity, String type) throws GException
   {
      List<T> ret = new LinkedList<>();
      for (Relation relation : getRelations(entity, Direction.INCOMING, type))
      {
         ret.add((T) relation.getStartEntity());
      }
      return ret;
   }

   /**
    * Gets the parent.
    *
    * @param <T> the generic type
    * @param entity the entity
    * @param type the type
    * @return the parent
    * @throws GException the g exception
    */
   @SuppressWarnings("unchecked")
   public <T extends Entity> T getParent(Entity entity, String type) throws GException
   {
      for (Relation relation : getRelations(entity, Direction.INCOMING, type))
      {
         return (T) relation.getStartEntity();
      }
      return null;
   }

   /**
    * Gets the parent relation.
    *
    * @param element the element
    * @param relation the relation
    * @return the group parent relation
    * @throws GException the g exception
    */
   public Relation getParentRelation(Entity element, String relation) throws GException
   {
      return getRelation(getParent(element, relation), element, relation);
   }

   /**
    * Gets the class entities.
    *
    * @param <T> the generic type
    * @param className the class name
    * @return the class entities
    * @throws GException the g exception
    */
   @SuppressWarnings("unchecked")
   public <T extends Entity> List<T> getClassEntities(String className) throws GException
   {
      List<Entity> list = classesMap.get(className);
      if (list != null)
      {
         return (List<T>) Collections.unmodifiableList(list);
      }
      else
      {
         return new LinkedList<T>();
      }
   }

   /**
    * Store key elements.
    *
    * @param element the element
    * @throws GException the g exception
    */
   private void storeKeyElements(Element element) throws GException
   {
      for (String key : keyAttributes)
      {
         String value = element.getAttributeValue(key);
         if (value != null)
         {
            Map<String, Element> stored = keyElementsMap.get(key);
            if (stored == null)
            {
               stored = new HashMap<String, Element>();
               keyElementsMap.put(key, stored);
            }
            Element exist = stored.get(value);
            if (exist != null)
            {
               throw new GException("Existing element for " + key + "=" + value);
            }
            stored.put(value, element);
         }
      }
   }

   public void addKeyAttribute(String key) throws GException
   {
      if (!keyAttributes.contains(key))
      {
         keyAttributes.add(key);
         for (Entity entity : entities)
         {
            storeKeyElements(entity);
         }
      }
      addRegisteredKey(key);
   }

   public void addRegisteredKey(String key) throws GException
   {
      Map<String, Element> stored = keyElementsMap.get(key);
      if (stored == null)
      {
         stored = new HashMap<String, Element>();
         keyElementsMap.put(key, stored);
         keyAttributes.add(key);
      }
   }

   /**
    * Removes the from key elements map.
    *
    * @param element the element
    */
   private void removeFromKeyElementsMap(Element element)
   {
      for (Map<String, Element> map : keyElementsMap.values())
      {
         if (map.containsValue(element))
         {
            for (String key : map.keySet())
            {
               if (map.get(key).equals(element))
               {
                  map.remove(key);
               }
            }
         }
      }
   }
   
   /**
    * Gets the key elements ids map.
    *
    * @return the key elements ids map
    */
   public Map<String, Map<String, String>> getKeyElementsIdsMap()
   {
      Map<String, Map<String, String>> ret = new HashMap<>();
      for ( String key : keyElementsMap.keySet())
      {
         Map<String, Element> originmap = keyElementsMap.get(key);
         Map<String, String> retinnermap = new HashMap<>();
         ret.put(key, retinnermap);

         for ( String subkey: originmap.keySet())
         {
            Element elt = originmap.get(subkey);
            retinnermap.put(subkey, Long.toString(elt.getIndex()));
         }
      }
      return ret;
   }

   /**
    * Update key element.
    *
    * @param key the key
    * @param element the element
    * @throws GException the g exception
    */
   void updateKeyElement(String key, Element element) throws GException
   {
      Map<String, Element> stored = keyElementsMap.get(key);
      if (stored == null)
      {
         stored = new HashMap<String, Element>();
         keyElementsMap.put(key, stored);
      }
      String value = element.getAttributeValue(key);
      stored.put(value, element);
   }

   /**
    * Gets the key element.
    *
    * @param key the key
    * @param value the value
    * @return the key element
    */
   public Element getKeyElement(String key, Object value)
   {
      Map<String, Element> stored = keyElementsMap.get(key);
      if (stored == null)
      {
         stored = new HashMap<String, Element>();
//         keyElementsMap.put(key, stored);
      }
      Element exist = stored.get(value);
      return exist;
   }

   /**
    * Gets the key relation.
    *
    * @param key the key
    * @param value the value
    * @return the key relation
    */
   public Relation getKeyRelation(String key, String value)
   {
      Element elt = getKeyElement(key, value);
      if (elt instanceof Relation)
      {
         return (Relation) elt;
      }
      return null;
   }

   /**
    * Gets the key entity.
    *
    * @param key the key
    * @param value the value
    * @return the key entity
    */
   public Entity getKeyEntity(String key, String value)
   {
      Element elt = getKeyElement(key, value);
      if (elt instanceof Entity)
      {
         return (Entity) elt;
      }
      return null;
   }
   
   /**
    * Checks if is free key value.
    *
    * @param key the key
    * @param value the value
    * @return true, if is free key value
    */
   public boolean isFreeKeyValue(String key, Object value)
   {
      return getKeyElement(key, value) == null;
   }
   
   /**
    * Checks if it is key value.
    *
    * @param key the key
    * @return true, if it is key value
    */
   public boolean isRegisteredKey(String key)
   {
      return keyElementsMap.get(key) != null;
   }

   public List<String> getClassProperties(Class<? extends GlideElement> theClass) throws GException
   {
      return getClassProperties(theClass.getName());
   }

   public List<String> getClassProperties(String className) throws GException
   {
      List<String> ret = new LinkedList<>();
      for ( Entity entity : getClassEntities(className))
      {
         GlideElement element = (GlideElement) entity;
         for ( String key : element.propsProperty().keySet())
         {
            if (!ret.contains(key))
            {
               ret.add(key);
            }
         }
      }
      Collections.sort(ret);
      return ret;
   }

   public List<String> getClassPropertyValues(Class<? extends GlideElement> theClass, String key) throws GException
   {
      return getClassPropertyValues(theClass.getName(), key);
   }

   public List<String> getClassPropertyValues(String className, String key) throws GException
   {
      List<String> ret = new LinkedList<>();
      for ( Entity entity : getClassEntities(className))
      {
         GlideElement element = (GlideElement) entity;
         String value = element.getProp(key);
         if (!ret.contains(value))
         {
            ret.add(value);
         }
      }
      Collections.sort(ret);
      return ret;
   }
   
   public void putInformation(String key, Object value)
   {
      information.put(key, value);
   }
   
   @SuppressWarnings("unchecked")
   public <T> T getInformation(String key)
   {
      return (T) information.get(key);
   }
   
   public long searchElementWhere()
   {
      return -1;
   }
}
