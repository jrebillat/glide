package net.alantea.glide.storex;

import java.io.File;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import net.alantea.glide.GException;
import net.alantea.glide.Glider;
import net.alantea.glide.IDriver;
import net.alantea.storex.Reader;
import net.alantea.storex.StorexClassLoader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

public class StorexDriver implements IDriver
{

   private AtomicBoolean freeToSave = new AtomicBoolean(true);

   @Override
   public Glider read(String path) throws GException
   {
      try
      {
         Glider glider = (Glider) Reader.load(path);
         return glider;
      }
      catch (StorexException e)
      {
         throw new GException(e);
      }
   }

   @Override
   public void write(String path, Glider glider, Map<String, String> keys) throws GException
   {
      if (freeToSave.compareAndSet(true, false))
      {
         try
         {
            Writer.write(path, glider, null, keys);
         }
         catch (StorexException e)
         {
            throw new GException(e);
         }
         freeToSave.set(true);
      }
   }

   
   /**
    * Adds the jar file.
    *
    * @param file the file
    */
   @Override
   public void addJarFile(File file)
   {
      StorexClassLoader.addJarFile(file);
   }
}
