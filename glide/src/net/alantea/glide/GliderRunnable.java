package net.alantea.glide;

@FunctionalInterface
public interface GliderRunnable
{
   public void execute(Glider glider);
}
