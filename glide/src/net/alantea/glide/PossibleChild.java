package net.alantea.glide;

public class PossibleChild
{
   private Glider glider;
   private String name;
   private Class<? extends GlideElement> elementClass;
   
   public PossibleChild(Glider glider, Class<? extends GlideElement> theClass)
   {
      this.glider = glider;
      this.elementClass = theClass;
      this.name = theClass.getSimpleName();
   }
   
   public String getSimpleName()
   {
      return name;
   }

   public GlideElement createInstance()
   {
      try
      {
         return glider.createEntity(elementClass);
      }
      catch (GException e)
      {
         e.printStackTrace();
      }
      return null;
   }

   public Class<? extends Element> getCreationClass()
   {
      return elementClass;
   }

}
