package net.alantea.glide;

/** Related persons. */
public class Related extends SimpleRelated
{
   /**  */
   private String reason = "friend";
   
   /** Constructor. */
   public Related() {
      reason = "friend";
   }


   /** -------------------------- number -------------------------- */
   /** Access to the property.
   @return the property */
   public final String getReason() { return this.reason; }
   
   /** Sets the  value.
   @param value the new value */
   public final void setReason(String value) { this.reason = value; }
}
