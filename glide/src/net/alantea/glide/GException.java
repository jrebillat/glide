package net.alantea.glide;

/**
 * The Class GException.
 */
@SuppressWarnings("serial")
public class GException extends Exception
{

   /**
    * Instantiates a new g exception.
    */
   public GException()
   {
   }

   /**
    * Instantiates a new g exception.
    *
    * @param message the message
    */
   public GException(String message)
   {
      super(message);
   }

   /**
    * Instantiates a new g exception.
    *
    * @param cause the cause
    */
   public GException(Throwable cause)
   {
      super(cause);
   }

   /**
    * Instantiates a new g exception.
    *
    * @param message the message
    * @param cause the cause
    */
   public GException(String message, Throwable cause)
   {
      super(message, cause);
   }

   /**
    * Instantiates a new g exception.
    *
    * @param message the message
    * @param cause the cause
    * @param enableSuppression the enable suppression
    * @param writableStackTrace the writable stack trace
    */
   public GException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
   {
      super(message, cause, enableSuppression, writableStackTrace);
   }
}
