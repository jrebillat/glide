package net.alantea.glide;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The Class RelationMap.
 */
@SuppressWarnings("serial")
public class RelationMap extends HashMap<Object, Object>
{
   
   /** The manager. */
   private Glider manager;
   
   /**
    * Instantiates a new relation map.
    *
    * @param start the start
    * @param direction the direction
    * @param type the type
    * @throws GException the database exception
    */
   public RelationMap(Entity start, Direction direction, String type) throws GException
   {
      manager = start.getGlider();
      
      List<Relation> relations = manager.getRelations(start, direction, type);
      for (Relation relation : relations)
      {
         put (relation, relation.getEndEntity());
      }
   }

   /**
    * Gets the keys.
    *
    * @param <T> the generic type
    * @return the keys
    * @throws GException the database exception
    */
   @SuppressWarnings("unchecked")
   <T> List<T> getKeys() throws GException
   {
      List<T> ret = new LinkedList<>();
      for (Object key : keySet())
      {
         try
         {
            ret.add((T) key);
         }
         catch (ClassCastException e)
         {
            throw new GException(e);
         }
      }
      return ret;
   }

   /**
    * Gets the values.
    *
    * @param <T> the generic type
    * @return the values
    * @throws GException the database exception
    */
   @SuppressWarnings("unchecked")
   <T> List<T> getValues() throws GException
   {
      List<T> ret = new LinkedList<>();
      for (Object key : values())
      {
         try
         {
            ret.add((T) key);
         }
         catch (ClassCastException e)
         {
            throw new GException(e);
         }
      }
      return ret;
   }
   
   /**
    * Order values by key.
    *
    * @param <T> the generic type
    * @param key the key
    * @return the list
    * @throws GException the g exception
    */
   @SuppressWarnings("unchecked")
   public <T> List<T> orderValuesByKey(String key) throws GException
   {
      final List<GException> raised = new ArrayList<>(1);
      List<T> ret = new LinkedList<>();
      List<Entry<Object, Object>>  entries = entrySet().stream().sorted(new Comparator<Map.Entry<Object, Object>>() {
         @SuppressWarnings("rawtypes")
         @Override
         public int compare(Entry<Object, Object> o1, Entry<Object, Object> o2)
         {
               try
               {
                  Field f1 = o1.getKey().getClass().getDeclaredField(key);
                  Field f2 = o2.getKey().getClass().getDeclaredField(key);
                  f1.setAccessible(true);
                  f2.setAccessible(true);
                  Object v1 = f1.get(o1.getKey());
                  Object v2 = f2.get(o2.getKey());
                  if ((v1 instanceof Comparable) && (v2 instanceof Comparable) )
                  {
                     return ((Comparable)v1).compareTo(v2);
                  }
               }
               catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e)
               {
                  e.printStackTrace();
                  if (raised.isEmpty())
                  {
                     raised.set(0, new GException(e));
                  }
               }
               return 0;
         }
      }).collect(Collectors.toList());
      
      if (!raised.isEmpty())
      {
         throw raised.get(0);
      }
      
      for (Entry<Object, Object> entry : entries)
      {
         ret.add((T) entry.getValue());
      }

      return ret;
   }
   
   /**
    * Order values by relation objects.
    *
    * @param <T> the generic type
    * @param key the key
    * @param comparator the comparator
    * @return the list
    * @throws GException the g exception
    */
   @SuppressWarnings("unchecked")
   public <T> List<T> orderValuesByRelationObjects(String key, FunctionalComparison comparator) throws GException
   {
      final List<GException> raised = new ArrayList<>(1);
      List<T> ret = new LinkedList<>();
      List<Entry<Object, Object>>  entries = entrySet().stream().sorted(new Comparator<Map.Entry<Object, Object>>() {
         @Override
         public int compare(Entry<Object, Object> o1, Entry<Object, Object> o2)
         {
            try
            {
               return comparator.compare(o1.getKey(), o2.getKey());
            }
            catch( Exception e)
            {
               if (raised.isEmpty())
               {
                  raised.set(0, new GException(e));
               }
               return 0;
            }
         }
      }).collect(Collectors.toList());
      
      if (!raised.isEmpty())
      {
         throw raised.get(0);
      }
      
      for (Entry<Object, Object> entry : entries)
      {
         ret.add((T) entry.getValue());
      }

      return ret;
   }
   
   /**
    * Order values by end objects.
    *
    * @param <T> the generic type
    * @param key the key
    * @param comparator the comparator
    * @return the list
    * @throws GException the g exception
    */
   @SuppressWarnings("unchecked")
   public <T> List<T> orderValuesByEndObjects(String key, FunctionalComparison comparator) throws GException
   {
      final List<GException> raised = new ArrayList<>(1);
      List<T> ret = new LinkedList<>();
      List<Entry<Object, Object>>  entries = entrySet().stream().sorted(new Comparator<Map.Entry<Object, Object>>() {
         @Override
         public int compare(Entry<Object, Object> o1, Entry<Object, Object> o2)
         {
            try
            {
               return comparator.compare(o1.getValue(), o2.getValue());
            }
            catch( Exception e)
            {
               if (raised.isEmpty())
               {
                  raised.set(0, new GException(e));
               }
               return 0;
            }
         }
      }).collect(Collectors.toList());
      
      if (!raised.isEmpty())
      {
         throw raised.get(0);
      }
      
      for (Entry<Object, Object> entry : entries)
      {
         ret.add((T) entry.getValue());
      }

      return ret;
   }
   
   /**
    * Order values by entries&lt;Relationship object, End object&gt;.
    *
    * @param <T> the generic type
    * @param key the key
    * @param comparator the comparator
    * @return the list
    * @throws GException the g exception
    */
   @SuppressWarnings("unchecked")
   public <T> List<T> orderValuesByEntries(String key, FunctionalComparison comparator) throws GException
   {
      final List<GException> raised = new ArrayList<>(1);
      List<T> ret = new LinkedList<>();
      List<Entry<Object, Object>>  entries = entrySet().stream().sorted(new Comparator<Map.Entry<Object, Object>>() {
         @Override
         public int compare(Entry<Object, Object> o1, Entry<Object, Object> o2)
         {
            try
            {
               return comparator.compare(o1, o2);
            }
            catch( Exception e)
            {
               if (raised.isEmpty())
               {
                  raised.set(0, new GException(e));
               }
               return 0;
            }
         }
      }).collect(Collectors.toList());
      
      if (!raised.isEmpty())
      {
         throw raised.get(0);
      }
      
      for (Entry<Object, Object> entry : entries)
      {
         ret.add((T) entry.getValue());
      }

      return ret;
   }
}
