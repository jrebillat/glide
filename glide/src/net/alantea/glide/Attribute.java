package net.alantea.glide;

/**
 * The Class Attribute. Should never be instantiated directly.
 */
public class Attribute
{
   
   /** The key. */
   private String key;

   /** The value. */
   private Object value;

   /**
    * Instantiates a new attribute. This constructor shall not be used outside the glider.
    */
   Attribute()
   {
      // Empty
   }

   /**
    * Instantiates a new attribute from a key. This constructor shall not be used outside the glider.
    *
    * @param key the key
    */
   protected Attribute(String key)
   {
      this.key = key;
   }

   /**
    * Gets the key.
    *
    * @return the key
    */
   public String getKey()
   {
      return key;
   }

   /**
    * Gets the value.
    *
    * @param <T> the generic type
    * @return the value
    */
   @SuppressWarnings("unchecked")
   public <T> T getValue()
   {
      return (T) value;
   }

   /**
    * Sets the value.
    *
    * @param value the new value
    */
   public void setValue(Object value)
   {
      this.value = value;
   }
   
   /* (non-Javadoc)
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object object)
   {
      if (object instanceof Attribute)
      {
         return ((key != null) && (key.equals(((Attribute) object).getKey())));
      }
      return false;
   }

   /* (non-Javadoc)
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode()
   {
      if (key == null)
      {
         return 0;
      }
      return key.hashCode();
   }
}
