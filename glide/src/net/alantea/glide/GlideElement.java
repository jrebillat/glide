/*
 * 
 */
package net.alantea.glide;
import java.util.ArrayList;
import java.util.List;

import net.alantea.liteprops.MapProperty;
import net.alantea.liteprops.StringProperty;


/** class to hold story entities base elements. */
public class GlideElement extends Entity implements Comparable<GlideElement>
{
   /** Element title. */
   private StringProperty name = new StringProperty();
   
   /** The properties. */
   private MapProperty<String, String> props = new MapProperty<>();
   
   /** The listeners. */
   private transient List<Changed> changedListeners = new ArrayList<>();
   
   /** Constructor. */
   public GlideElement() {}
   
   public String toString()
   {
      return name.get();
   }
   
   @Override
   public int compareTo(GlideElement element)
   {
      return this.getName().compareTo(element.getName());
   }

   /** -------------------------- name -------------------------- */
   /** Access to the name property.
   @return the name keys property */
   public final StringProperty nameProperty() { return this.name; }

   /** Gets the name value.
   @return the name value */
   public final String getName() { return this.name.get(); }

   /** Sets the name value.
   @param value the new name value */
   public final void setName(String value) {
      this.name.set(value);
   }

   //  -------------------------- properties --------------------------
   /** Access to the properties property.
    * @return the map property */
   public final MapProperty<String, String> propsProperty() { return this.props; }

   /**
    *  Gets the properties value.
    *
    * @param key the key
    * @return the properties value
    */
   public final String getProp(String key) { return this.props.get(key); }

   /**
    *  Put a value.
    *
    * @param key the key
    * @param value the new name value
    */
   public final void setProp(String key, String value) {
      this.props.put(key, value);
   }

   //  -------------------------- Listeners --------------------------
   /**
    * Adds the listener.
    *
    * @param listener the listener
    */
   public void addChangedListener(Changed listener)
   {
      if (!changedListeners.contains(listener))
      {
         changedListeners.add(listener);
      }
   }
   
   /**
    * Removes the listener.
    *
    * @param listener the listener
    */
   public void removeChangedListener(Changed listener)
   {
      if (changedListeners.contains(listener))
      {
         changedListeners.remove(listener);
      }
   }

   /**
    * Fire the change.
    *
    */
   protected void fireChanged()
   {
      for (Changed listener : changedListeners)
      {
         try
         {
            listener.changed(this);
         }
         catch (Exception e)
         {
         }
      }
   }
}
