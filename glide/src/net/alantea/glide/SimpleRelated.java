package net.alantea.glide;

/** Related persons. */
public class SimpleRelated extends Relation
{
   private String description = "";
   
   /** Constructor. */
   public SimpleRelated() {}


   /** -------------------------- description -------------------------- */
   /** Access to the description.
   @return the description */
   public final String getDescription() { return this.description; }
}
