package net.alantea.glide.xml;

import java.util.List;

import net.alantea.glide.Entity;
import net.alantea.glide.Glider;

public class TypedEntity extends Entity
{
   private static final String ATTR_TYPE = "__TypedEntity_Type__";
   
   public TypedEntity(String type)
   {
      setLabel(ATTR_TYPE + type);
   }
   
   public static List<Entity> getTypedEntities(Glider glider, String type)
   {
      return glider.getLabeledEntities(ATTR_TYPE + type);
   }
}
