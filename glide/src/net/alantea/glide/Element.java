package net.alantea.glide;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import net.alantea.liteprops.ChangeListener;
import net.alantea.liteprops.IBinding;

/**
 * The Class Element.
 */
public abstract class Element implements IBinding
{
   
   /** The Constant REASON_INDEX. */
   private static final String REASON_INDEX = "__index__";

   /** The Constant REASON_ATTRIBUTE. */
   private static final Object REASON_ATTRIBUTE = "__attribute__";
   
   /** The index. */
   private long index;
   
   /** The attributes. */
   private List<Attribute> attributes = new LinkedList<>();

   /** The glider. */
   private transient Glider glider;
   
   /** The attributes map. */
   private transient Map<String, Object> attributesMap = null;
   
   /** The attributes reverse map. */
   private transient Map<String, Attribute> attributesReverseMap = null;

   /** The listeners. */
   private transient List<ChangeListener<Object>> listeners = new LinkedList<>();

   /**
    * Instantiates a new element.
    */
   protected Element()
   {
   }

   /**
    * Gets the listeners.
    *
    * @return the listeners
    */
   public List<ChangeListener<Object>> getListeners()
   {
      return listeners ;
   }
   
   /**
    * On creation done.
    */
   protected void onCreationDone() {}
   
   //------------------------------------------------------------------------------
   // Database management

   /**
    * Gets the index.
    *
    * @return the index
    */
   public final long getIndex()
   {
      return index;
   }

   /**
    * Sets the index.
    *
    * @param index the new index
    */
   void setIndex(long index)
   {
      this.index = index;
      fireChanged(null, REASON_INDEX);
   }

   /**
    * Gets the glider.
    *
    * @return the glider
    */
   public Glider getGlider()
   {
      return glider;
   }

   /**
    * Sets the glider.
    *
    * @param glider the new glider
    */
   void setGlider(Glider glider)
   {
      this.glider = glider;
   }
   
   //------------------------------------------------------------------------------
   // Attributes management
   
   /**
    * Gets the attribute keys.
    *
    * @return the attribute keys
    */
   public final List<String> getAttributeKeys()
   {
      return attributes.stream()
      .map(Attribute::getKey)
      .collect(Collectors.toList());
   }
   
   /**
    * Gets the attribute value.
    *
    * @param <T> the generic type
    * @param key the key
    * @return the attribute value
    */
   @SuppressWarnings("unchecked")
   public final <T> T getAttributeValue(String key)
   {
      verifyMap();
      return (T) attributesMap.get(key);
   }
   
   /**
    * Sets the attribute.
    *
    * @param key the key
    * @param value the value
    * @throws GException the g exception
    */
   public final void setAttribute(String key, Object value) throws GException
   {
      if (value == null)
      {
         Attribute attr = attributesReverseMap.get(key);
         if (attr != null)
         {
            attributes.remove(attr);
            attributesMap = null;
            verifyMap();
            getGlider().updateKeyElement(key, this);
         }
      }
      else if (!getGlider().isRegisteredKey(key))
      {
         // TODO
         verifyMap();
         Attribute attr = attributesReverseMap.get(key);
         if (attr == null)
         {
            attr = new Attribute(key);
            attributes.add(attr);
            attributesReverseMap.put(key, attr);
         }
         attr.setValue(value);
         attributesMap.put(key, value);
      }
      else if (getGlider().isFreeKeyValue(key, value))
      {
         verifyMap();
         Attribute attr = attributesReverseMap.get(key);
         if (attr == null)
         {
            attr = new Attribute(key);
            attributes.add(attr);
            attributesReverseMap.put(key, attr);
         }
         attr.setValue(value);
         attributesMap.put(key, value);
         getGlider().updateKeyElement(key, this);
      }
      else
      {
         throw new GException("value already used for registered key");
      }
      fireChanged(null, REASON_ATTRIBUTE);
   }
   
   public boolean hasValue(String key, String value)
   {
      String val = getAttributeValue(key);
      if ((val != null) && (val.equals(value)))
      {
         return true;
      }
      return false;
   }
   
   /**
    * Verify map.
    */
   private void verifyMap()
   {
      if (attributesMap == null)
      {
         attributesMap = new HashMap<>();
         attributesReverseMap = new HashMap<>();
         for (Attribute attr : attributes)
         {
            attributesMap.put(attr.getKey(), attr.getValue());
            attributesReverseMap.put(attr.getKey(), attr);
         }
      }
   }

   /* (non-Javadoc)
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object object)
   {
      if ((object instanceof Element) && (this.getClass().equals(object.getClass())))
      {
         return ((index > 0) && (index == (((Element) object).getIndex())));
      }
      return false;
   }

   /* (non-Javadoc)
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode()
   {
     return Long.hashCode(index) + getClass().hashCode();
   }
}
