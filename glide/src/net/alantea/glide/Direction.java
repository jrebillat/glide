package net.alantea.glide;

/**
 * The Enum Direction.
 */
public enum Direction
{
   
   /** The incoming direction. */
   INCOMING,
   
   /** The outgoing direction. */
   OUTGOING,
   
   /** Both directions. */
   BOTH
}
