package net.alantea.glide;

import java.util.LinkedList;
import java.util.List;

/**
 * The Class Relation.
 */
public class OrderedRelation extends Relation
{
   /** The order. */
   private long order = Long.MAX_VALUE;
   /**
    * Instantiates a new relation.
    */
   public OrderedRelation()
   {
   }

   /**
    * Gets the order.
    *
    * @return the order
    */
   public long getOrder()
   {
      return order;
   }

   /**
    * Sets the order.
    *
    * @param order the new order
    */
   void setOrder(long order)
   {
      List<Relation> relations = null;
      try
      {
         relations = getGlider().getRelations(getStartEntity(), Direction.OUTGOING, getType());
      }
      catch (GException e)
      {
         e.printStackTrace();
         return;
      }
      
      List<OrderedRelation> orelations = new LinkedList<>();
      for (Relation relation : relations)
      {
         orelations.add((OrderedRelation) relation);
      }
      // We will consider that the relations are OrderedRelations and already ordered
      // First : remove the relation from list with index shifting
      boolean found = false;
      for (OrderedRelation orelation : orelations)
      {
         if (!found)
         {
            if (orelation.equals(this))
            {
               found = true;
               relations.remove(this);
            }
         }
         else
         {
            orelation.order = orelation.order -1;
         }
      }
      
      // remove from working list
      orelations.remove(this);
      
      // reinsert in list at new order index
      found = false;
      long maxIndex = 0;
      for (OrderedRelation orelation : orelations)
      {
         if (orelation.order != maxIndex)
         {
            orelation.order = maxIndex;
         }
         if (orelation.order < order)
         {
         }
         else if (orelation.order == order)
         {
            found = true;
            orelation.order = orelation.order +1;
            int idx = relations.indexOf(orelation);
            relations.add(idx, this);
            this.order = order;
         }
         else
         {
            orelation.order = orelation.order + 1;
         }
         maxIndex++;
      }
      
      // At the end
      if (!found)
      {
         relations.add(this);
         this.order = maxIndex;
      }
   }

}
