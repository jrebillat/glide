package net.alantea.glide;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The Class Entity.
 */
public class Entity extends Element
{
   
   /** The Constant REASON_LABEL. */
   private static final Object REASON_LABEL = "__label__";

   /** The Constant REASON_RELATION. */
   private static final Object REASON_RELATION = "__relation__";

   /** The incoming relations. */
   private transient List<Relation> incomingRelations = new LinkedList<>();
   
   /** The outgoing relations. */
   private transient List<Relation> outgoingRelations = new LinkedList<>();
   
   /** The labels. */
   private List<String> labels = new LinkedList<>();

   /**
    * Instantiates a new entity.
    */
   public Entity()
   {
      incomingRelations = new LinkedList<>();
      outgoingRelations = new LinkedList<>();
   }

   //------------------------------------------------------------------------------
   // Relation management
   
   /**
    * Gets the relations.
    *
    * @param direction the direction
    * @return the relations
    */
   public List<Relation> getRelations(Direction direction)
   {
      List<Relation> ret = new LinkedList<>();
      if ((direction == Direction.INCOMING) || (direction == Direction.BOTH))
      {
         ret.addAll(incomingRelations);
      }
      if ((direction == Direction.OUTGOING) || (direction == Direction.BOTH))
      {
         ret.addAll(outgoingRelations);
      }
      return ret;
   }

   /**
    * Gets the relations.
    *
    * @param direction the direction
    * @param type the type
    * @return the relations
    */
   public List<Relation> getRelations(Direction direction, String type)
   {
      List<Relation> ret = new LinkedList<>();
      if (type == null)
      {
         return ret;
      }
      if ((direction == Direction.INCOMING) || (direction == Direction.BOTH))
      {
         ret.addAll(reorder(incomingRelations.stream()
               .filter(relation -> type.equals(relation.getType()))
               .collect(Collectors.toList())));
         
      }
      if ((direction == Direction.OUTGOING) || (direction == Direction.BOTH))
      {
         ret.addAll(reorder(outgoingRelations.stream()
               .filter(relation -> type.equals(relation.getType()))
               .collect(Collectors.toList())));
      }
      return ret;
   }

   private Collection<? extends Relation> reorder(List<Relation> collect)
   {
      if (!collect.isEmpty())
      {
         Relation ref = collect.get(0);
         if (ref instanceof OrderedRelation)
         {
            int vmin = Integer.MAX_VALUE;
            int vmax = 0;
            for (Relation rel : collect)
            {
               vmax = Math.max(vmax, (int) ((OrderedRelation) rel).getOrder());
               vmin = Math.min(vmin, (int) ((OrderedRelation) rel).getOrder());
            }
            vmax++;
            
            OrderedRelation[] arr = new OrderedRelation[vmax - vmin];
            for (Relation rel : collect)
            {
               arr[(int) ((OrderedRelation) rel).getOrder() - vmin] = (OrderedRelation)rel;
            }
            
            List<Relation> ret = new LinkedList<>();
            for (int i = 0; i < vmax - vmin; i++)
            {
               if (arr[i] != null)
               {
                  ret.add(arr[i]);
               }
            }
            return ret;
         }
      }
      return collect;
   }

   //------------------------------------------------------------------------------
   // Label management
   
   /**
    * Sets the label.
    *
    * @param labelName the new label
    */
   public void setLabel(String labelName)
   {
      if (!labels.contains(labelName))
      {
         Label label = getGlider().createLabel(labelName);
         labels.add(labelName);
         label.addEntity(this);
         fireChanged(null, REASON_LABEL);
      }
   }
   
   /**
    * Sets the label.
    *
    * @param label the new label
    */
   public void setLabel(Label label)
   {
      if (!labels.contains(label.getName()))
      {
         labels.add(label.getName());
         label.addEntity(this);
         fireChanged(null, REASON_LABEL);
      }
   }
   
   /**
    * Removes the label.
    *
    * @param label the label
    */
   public void removeLabel(Label label)
   {
      if (labels.contains(label.getName()))
      {
         labels.remove(label.getName());
         label.removeEntity(this);
         fireChanged(null, REASON_LABEL);
      }
   }

   /**
    * Checks for label.
    *
    * @param label the label
    * @return true, if successful
    */
   public boolean hasLabel(String label)
   {
      return labels.contains(label);
   }
   
   /**
    * Gets the labels.
    *
    * @return the labels
    */
   public List<String> getLabels()
   {
      return Collections.unmodifiableList(labels);
   }
   
   /**
    * Adds the incoming relation.
    *
    * @param relation the relation
    */
   void addIncomingRelation(Relation relation)
   {
      if (incomingRelations == null)
      {
         incomingRelations = new LinkedList<>();
      }
      incomingRelations.add(relation);
      fireChanged(null, REASON_RELATION);
   }
   
   /**
    * Removes the incoming relation.
    *
    * @param relation the relation
    */
   void removeIncomingRelation(Relation relation)
   {
      if (incomingRelations == null)
      {
         incomingRelations = new LinkedList<>();
      }
      incomingRelations.remove(relation);
      fireChanged(null, REASON_RELATION);
   }
   
   /**
    * Adds the outgoing relation.
    *
    * @param relation the relation
    */
   void addOutgoingRelation(Relation relation)
   {
      if (outgoingRelations == null)
      {
         outgoingRelations = new LinkedList<>();
      }
      outgoingRelations.add(relation);
      fireChanged(null, REASON_RELATION);
   }
   
   /**
    * Removes the outgoing relation.
    *
    * @param relation the relation
    */
   void removeOutgoingRelation(Relation relation)
   {
      if (outgoingRelations == null)
      {
         outgoingRelations = new LinkedList<>();
      }
      outgoingRelations.remove(relation);
      fireChanged(null, REASON_RELATION);
   }
}
