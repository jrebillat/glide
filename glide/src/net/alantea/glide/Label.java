package net.alantea.glide;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class Label.
 */
public class Label
{
   
   /** The entities. */
   private transient List<Entity> entities = new LinkedList<>();
   
   /** The name. */
   private String name;

   /**
    * Instantiates a new label.
    */
   @SuppressWarnings("unused")
   private Label()
   {
   }
   
   /**
    * Instantiates a new label.
    *
    * @param name the name
    */
   public Label(String name)
   {
      this.name = name;
   }

   /**
    * Adds the entity.
    *
    * @param entity the entity
    */
   void addEntity(Entity entity)
   {
      if (!entities.contains(entity))
      {
         entities.add(entity);
      }
   }

   /**
    * Removes the entity.
    *
    * @param entity the entity
    */
   void removeEntity(Entity entity)
   {
      if (entities.contains(entity))
      {
         entities.remove(entity);
      }
   }

   /**
    * Gets the entities.
    *
    * @return the entities
    */
   public List<Entity> getEntities()
   {
      return Collections.unmodifiableList(entities);
   }

   /**
    * Gets the name.
    *
    * @return the name
    */
   public String getName()
   {
      return name;
   }
   
   /* (non-Javadoc)
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object object)
   {
      if (object instanceof Label)
      {
         return ((name != null) && (name.equals(((Label) object).getName())));
      }
      return false;
   }

   /* (non-Javadoc)
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode()
   {
     return name.hashCode();
   }
}
