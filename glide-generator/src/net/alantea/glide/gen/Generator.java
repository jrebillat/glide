package net.alantea.glide.gen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.alantea.glide.gen.internal.GeneratorContainer;
import net.alantea.glide.gen.internal.GeneratorElement;
import net.alantea.glide.gen.internal.GeneratorEntity;
import net.alantea.glide.gen.internal.GeneratorField;
import net.alantea.glide.gen.internal.GeneratorPackage;
import net.alantea.glide.gen.internal.GeneratorRelation;
import net.alantea.glide.gen.internal.GeneratorRelationType;
import net.alantea.glide.gen.internal.GeneratorRelation.Multiple;

public class Generator extends DefaultHandler
{
   
   /** The parser. */
   private SAXParser parser;
   
   /** The entities. */
   private Map<String, GeneratorEntity> generatorEntities = new HashMap<>();

   private Map<String, GeneratorRelation> generatorRelations = new HashMap<>();

   private Map<String, GeneratorRelationType> generatorRelationTypes = new HashMap<>();
   
   private Stack<GeneratorElement> generatorElements = new Stack<>();
   
   private String projectPath;
   
   GeneratorEntity currentEntity = null;
   
   GeneratorRelationType currentRelationType = null;

   private GeneratorPackage currentPackage = null;
   
   private String characters = "";

   private GeneratorElement currentElement;

   private Generator(String projectPath) throws SAXException
   {
      this.projectPath = projectPath;
      SAXParserFactory spfac = SAXParserFactory.newInstance();

      try
      {
         parser = spfac.newSAXParser();
      }
      catch (ParserConfigurationException e)
      {
         throw new SAXException(e);
      }
   }

   public static void main(String[] args)
   {
      if (args.length > 1)
      {
         try
         {
            Generator generator = new Generator(args[1]);
            generator.parser.parse(args[0], generator);
            generator.create();
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
         catch (SAXException e)
         {
            e.printStackTrace();
         }
      }
   }
   

   private void create() throws IOException
   {
      for (GeneratorRelationType generatorRelationType : generatorRelationTypes.values())
      {
         String className = setFirstUppercase(generatorRelationType.getName());
         GeneratorPackage pack = generatorRelationType.getPackage();
         File dir = new File(projectPath + "/" + ((pack==null)?"":pack.getPath()));
         dir.mkdirs();
         Writer writer = new FileWriter(projectPath + "/" + ((pack==null)?"":pack.getPath()) + className + ".java");
         if (pack != null)
         {
            writer.write("package " + pack.getPackage() + ";\r\n");
         }
         writer.write("import net.alantea.glide.Relation;\r\n"
               + "import javafx.beans.property.StringProperty;\r\n"
               + "import javafx.beans.property.SimpleStringProperty;\r\n"
               + "import javafx.beans.property.IntegerProperty;\r\n"
               + "import javafx.beans.property.SimpleIntegerProperty;\r\n"
               + "import javafx.beans.property.LongProperty;\r\n"
               + "import javafx.beans.property.SimpleLongProperty;\r\n"
               + "import javafx.beans.property.DoubleProperty;\r\n"
               + "import javafx.beans.property.SimpleDoubleProperty;\r\n"
               + "import javafx.beans.property.FloatProperty;\r\n"
               + "import javafx.beans.property.SimpleFloatProperty;\r\n"
               + "import javafx.beans.property.BooleanProperty;\r\n"
               + "import javafx.beans.property.SimpleBooleanProperty;\r\n"
               + "import net.alantea.glide.Relation;\r\n"
               + "\r\n"
               + "/** " + generatorRelationType.getJavadoc() + " */\r\n"
               + "public class " + className + " extends Relation\r\n"
               + "{\r\n");

         // field declaration
         setFields(writer, generatorRelationType);

         writer.write("   /** Constructor. */\r\n");
         writer.write("   public " + className + "() {}\r\n\r\n");
         
         // field getters and setters
         setAccessors(writer, generatorRelationType);
         
         writer.write("}\r\n");
         writer.close();
      }
      
      for (GeneratorEntity generatorEntity : generatorEntities.values())
      {
         String className = setFirstUppercase(generatorEntity.getName());
         GeneratorPackage pack = generatorEntity.getPackage();
         File dir = new File(projectPath + "/" + ((pack==null)?"":pack.getPath()));
         dir.mkdirs();
         Writer writer = new FileWriter(projectPath + "/" + ((pack==null)?"":pack.getPath()) + className + ".java");
         if (pack != null)
         {
            writer.write("package " + pack.getPackage() + ";\r\n");
         }
         writer.write("import java.util.List;\r\n"
               + "import net.alantea.glide.GException;\r\n"
               + "import net.alantea.glide.Relation;\r\n"
               + "import net.alantea.glide.OrderedRelation;\r\n"
               + "import net.alantea.glide.Direction;\r\n"
               + "import net.alantea.glide.Entity;\r\n"
               + "import javafx.beans.property.StringProperty;\r\n"
               + "import javafx.beans.property.SimpleStringProperty;\r\n"
               + "import javafx.beans.property.IntegerProperty;\r\n"
               + "import javafx.beans.property.SimpleIntegerProperty;\r\n"
               + "import javafx.beans.property.LongProperty;\r\n"
               + "import javafx.beans.property.SimpleLongProperty;\r\n"
               + "import javafx.beans.property.DoubleProperty;\r\n"
               + "import javafx.beans.property.SimpleDoubleProperty;\r\n"
               + "import javafx.beans.property.FloatProperty;\r\n"
               + "import javafx.beans.property.SimpleFloatProperty;\r\n"
               + "import javafx.beans.property.BooleanProperty;\r\n"
               + "import javafx.beans.property.SimpleBooleanProperty;\r\n"
               + "\r\n"
               + "/** " + generatorEntity.getJavadoc() + " */\r\n"
               + "public class " + setFirstUppercase(className) + " extends Entity\r\n"
               + "{\r\n");

         // field declaration
         setFields(writer, generatorEntity);

         writer.write("\r\n   /** Constructor. */\r\n");
         writer.write("   public " + className + "() {}\r\n\r\n");

         // field getters and setters
         setAccessors(writer, generatorEntity);

         // children
         for (GeneratorRelation generatorRelation : generatorRelations.values())
         {
            if (generatorRelation.getFrom().equals(generatorEntity.getName()))
            {
               if ((generatorRelation.getMultiple() == Multiple.CHILDREN) || (generatorRelation.getMultiple() == Multiple.BOTH))
               {
                  writer.write("\r\n   /** " + setFirstUppercase(generatorRelation.getName()) + " children list property accessor. */\r\n");
                  writer.write("   public List<" + setFirstUppercase(generatorRelation.getTo()) + "> get" + setFirstUppercase(generatorRelation.getTo()) + "s() throws GException {"
                        + " return getGlider().getChildren(this, \"" + generatorRelation.getType() + "\"); }\r\n");
                  writer.write("   /** Adding a " + setFirstUppercase(generatorRelation.getName()) + " to the list. */\r\n");
                  if (generatorRelation.getOrdered())
                  {
                     writer.write("   public void add" + setFirstUppercase(generatorRelation.getTo()) + "( " + generatorRelation.getTo() + " child) throws GException {"
                           + " getGlider().createOrderedRelation(this, child, \"" + generatorRelation.getType() + "\", Integer.MAX_VALUE);}\r\n");
                     writer.write("   public void insert" + setFirstUppercase(generatorRelation.getTo()) + "( " + generatorRelation.getTo() + " child, int position) throws GException {"
                           + " getGlider().createOrderedRelation(this, child, \"" + generatorRelation.getType() + "\", position);}\r\n");
                  }
                  else
                  {
                     writer.write("   public void add" + setFirstUppercase(generatorRelation.getTo()) + "( " + generatorRelation.getTo() + " child) throws GException {"
                        + " getGlider().createRelation(this, child, \"" + generatorRelation.getType() + "\");}\r\n");
                  }
                  writer.write("   /** Removing a " + setFirstUppercase(generatorRelation.getName()) + " from the list. */\r\n");
                  writer.write("   public void remove" + setFirstUppercase(generatorRelation.getTo()) + "( " + generatorRelation.getTo() + " child) throws GException {"
                        + " Relation relation = getGlider().getRelation(this, child,\"" + generatorRelation.getType() + "\");"
                        + "getGlider().removeRelation(relation);}\r\n");
                  writer.write("   /** " + setFirstUppercase(generatorRelation.getName()) + " relations list property accessor. */\r\n");
                  writer.write("   public List<" + setFirstUppercase(generatorRelation.getType()) + "> get" + setFirstUppercase(generatorRelation.getName()) + "s() throws GException {"
                        + " return getGlider().getRelations(this, Direction.OUTGOING, \"" + generatorRelation.getType() + "\");}\r\n");
               }
               else
               {
                  writer.write("\r\n   /** " + setFirstUppercase(generatorRelation.getName()) + " property accessor. */\r\n");
                  writer.write("   public " + setFirstUppercase(generatorRelation.getTo()) + " get" + setFirstUppercase(generatorRelation.getTo()) + "() throws GException {"
                        + " return getGlider().getChild(this, \"" + generatorRelation.getType() + "\"); }\r\n");
                  writer.write("   /** Setting the " + setFirstUppercase(generatorRelation.getName()) + " value. */\r\n");
                  writer.write("   public void set" + setFirstUppercase(generatorRelation.getTo()) + "( " + generatorRelation.getTo() + " child) throws GException {"
                        + "   getGlider().removeRelations(this, \"" + generatorRelation.getType() + "\");"
                        + "   getGlider().createRelation(this, child, \"" + generatorRelation.getType() + "\");}\r\n");
               }
            }
            else if (generatorRelation.getTo().equals(generatorEntity.getName()))
            {
               if ((generatorRelation.getMultiple() == Multiple.PARENTS) || (generatorRelation.getMultiple() == Multiple.BOTH))
               {
                  writer.write("\r\n   /** " + setFirstUppercase(generatorRelation.getName()) + " parent list accessor. */\r\n");
                  writer.write("   public List<" + setFirstUppercase(generatorRelation.getFrom()) + "> get" + setFirstUppercase(generatorRelation.getFrom()) + "Parents() throws GException {"
                        + " return getGlider().getParents(this, \"" + generatorRelation.getType() + "\"); }\r\n");
                  writer.write("   public List<" + setFirstUppercase(generatorRelation.getType()) + "> get" + setFirstUppercase(generatorRelation.getName()) + "ParentRelations() throws GException {"
                        + " return getGlider().getRelations(this, Direction.INCOMING, \"" + generatorRelation.getType() + "\");}\r\n");
               }
               else
               {
                  writer.write("   public " + setFirstUppercase(generatorRelation.getFrom()) + " get" + setFirstUppercase(generatorRelation.getFrom()) + "Parent() throws GException {"
                        + " return getGlider().getParent(this, \"" + generatorRelation.getType() + "\"); }\r\n");
                  writer.write("   public " + setFirstUppercase(generatorRelation.getType()) + " get" + setFirstUppercase(generatorRelation.getName()) + "ParentRelation() throws GException {"
                        + " return getGlider().getRelation(getGlider().getParent(this, \"" + generatorRelation.getType() + "\"), this, \"" + generatorRelation.getType() + "\");}\r\n");
               }
            }
         }
         
         writer.write("}\r\n");
         writer.close();
      }
   }
   
   private void setFields(Writer writer, GeneratorContainer generatorContainer) throws IOException
   {
      for (GeneratorField generatorField : generatorContainer.getFields())
      {
         switch(generatorField.getType().toLowerCase())
         {
            case "string" :
               writeField(writer, generatorField, "String", "String");
               break;
            case "integer" :
               writeField(writer, generatorField, "Integer", "int");
               break;
            case "long" :
               writeField(writer, generatorField, "Long", "long");
               break;
            case "float" :
               writeField(writer, generatorField, "Float", "float");
               break;
            case "double" :
               writeField(writer, generatorField, "Double", "double");
               break;
            case "boolean" :
               writeField(writer, generatorField, "Boolean", "boolean");
               break;
         }
      }
   }
   
   private void writeField(Writer writer, GeneratorField generatorField, String type, String simpleType) throws IOException
   {
      if (generatorField.isProperty())
      {
         writer.write("   /** " + generatorField.getJavadoc() + " */\r\n");
         writer.write("   private " + type + "Property " + setFirstLowercase(generatorField.getName()) + " = new Simple" + type + "Property();\r\n");
      }
      else
      {
         writer.write("   /** " + generatorField.getJavadoc() + " */\r\n");
         writer.write("   private " + simpleType + " " + setFirstLowercase(generatorField.getName()) + ";\r\n");
      }
   }
   
   private void setAccessors(Writer writer, GeneratorContainer generatorContainer) throws IOException
   {
      for (GeneratorField generatorField : generatorContainer.getFields())
      {
         switch(generatorField.getType().toLowerCase())
         {
            case "string" :
               writeAccessor(writer, generatorField, "String", "StringProperty");
               break;
            case "integer" :
               writeAccessor(writer, generatorField, "int", "IntegerProperty");
               break;
            case "long" :
               writeAccessor(writer, generatorField, "long", "LongProperty");
               break;
            case "float" :
               writeAccessor(writer, generatorField, "float", "FloatProperty");
               break;
            case "double" :
               writeAccessor(writer, generatorField, "double", "DoubleProperty");
               break;
            case "boolean" :
               writeAccessor(writer, generatorField, "boolean", "BooleanProperty");
               break;
         }
      }
   }
   
   private void writeAccessor(Writer writer, GeneratorField generatorField, String type, String propertyType) throws IOException
   {
      String name = generatorField.getName();
      if (generatorField.isProperty())
      {
      writer.write("   public final " + propertyType + " " + setFirstLowercase(name) + "Property() { return this." + setFirstLowercase(name) + "; }\r\n");
      writer.write("   public final " + type + " get" + setFirstUppercase(name) + "() { return this." + setFirstLowercase(name) + ".get(); }\r\n");
      writer.write("   public final void set" + setFirstUppercase(name) + "(" + type + " value) { this." + setFirstLowercase(name) + ".set(value); }\r\n");
      }
      else
      {
         writer.write("   public final " + type + " get" + setFirstUppercase(name) + "() { return this." + setFirstLowercase(name) + "; }\r\n");
         writer.write("   public final void set" + setFirstUppercase(name) + "(" + type + " value) { this." + setFirstLowercase(name) + " = value; }\r\n");
      }
   }

   @Override
   public final void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
   {
      if (currentElement != null)
      {
         currentElement.setJavadoc(characters);
      }
      characters = "";
      switch(qName.toLowerCase())
      {
         case "package" :
            currentPackage = new GeneratorPackage();
            currentPackage.setPackage(attributes.getValue("package"));
            new File(currentPackage.getPath()).mkdirs();
            generatorElements.push(currentPackage);
            break;
         case "entity" :
            currentEntity = new GeneratorEntity();
            currentEntity.setName(attributes.getValue("class"));
            currentEntity.setPackage(currentPackage);
            generatorElements.push(currentEntity);
            break;

         case "relationtype" :
            currentRelationType = new GeneratorRelationType();
            currentRelationType.setName(attributes.getValue("name"));
            currentRelationType.setPackage(currentPackage);
            generatorElements.push(currentRelationType);
            break;
            
         case "relation" :
            GeneratorRelation generatorRelation = new GeneratorRelation();
            generatorRelation.setName(attributes.getValue("name"));
            generatorRelation.setType(attributes.getValue("type"));
            generatorRelation.setFrom(attributes.getValue("from"));
            generatorRelation.setTo(attributes.getValue("to"));
            generatorRelation.setOrdered(attributes.getValue("ordered"));
            String multiple = attributes.getValue("multiple");
            if (multiple != null)
            {
               switch(multiple.toUpperCase())
               {
                  case "CHILDREN" :
                     generatorRelation.setMultiple(Multiple.CHILDREN);
                     break;

                  case "PARENTS" :
                     generatorRelation.setMultiple(Multiple.PARENTS);
                     break;

                  case "BOTH" :
                     generatorRelation.setMultiple(Multiple.BOTH);
                     break;
                     
                  default :
                     generatorRelation.setMultiple(Multiple.NONE);
               }
            }
            generatorRelations.put(generatorRelation.getName(), generatorRelation);
            generatorElements.push(generatorRelation);
            break;

         case "field" :
            GeneratorField generatorField = new GeneratorField();
            generatorField.setName(attributes.getValue("name"));
            generatorField.setType(attributes.getValue("type"));
            if (attributes.getValue("property") != null)
            {
               generatorField.setProperty(Boolean.parseBoolean(attributes.getValue("property")));
            }
            if (currentEntity != null)
            {
               currentEntity.add(generatorField);
            }
            else if (currentRelationType != null)
            {
               currentRelationType.add(generatorField);
            }
            generatorElements.push(generatorField);
            break;
            
         default :
            generatorElements.push(null);
      }
      currentElement = generatorElements.peek();
   }
   
   @Override
   public final void endElement(String uri, String localName, String qName) throws SAXException
   {
      GeneratorElement generatorElement = generatorElements.pop();
      if (generatorElement != null)
      {
         generatorElement.setJavadoc(characters);
         characters = "";
      }
      switch(qName.toLowerCase())
      {
         case "entity" :
            generatorEntities.put(currentEntity.getName(), currentEntity);
            currentEntity = null;
            break;
            
         case "relationtype" :
            generatorRelationTypes.put(currentRelationType.getName(), currentRelationType);
            currentRelationType = null;
            break;
            
         case "package" :
            currentPackage = null;
            break;
      }
      if (!generatorElements.isEmpty())
      {
         currentElement = generatorElements.peek();
      }
      else
      {
         currentElement = null;
      }
   }
   
   public void characters(char ch[],int start,int length)
   {      
      characters += (new String(ch,start,length));
   } 
   
   private String setFirstUppercase(String origin)
   {
      return origin.substring(0, 1).toUpperCase() + origin.substring(1);
   }
   
   private String setFirstLowercase(String origin)
   {
      return origin.substring(0, 1).toLowerCase() + origin.substring(1);
   }
}
