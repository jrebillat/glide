package net.alantea.glide.gen.internal;

public class GeneratorPackage extends GeneratorElement
{
   private String path;
   
   private String pack;

   public String getPath()
   {
      return path;
   }

   public String getPackage()
   {
      return pack;
   }

   public void setPackage(String pack)
   {
      this.pack = pack;
      path = "src/" + pack.replaceAll("\\.", "/") + "/";
   }

}
