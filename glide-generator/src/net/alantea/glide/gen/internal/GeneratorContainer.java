package net.alantea.glide.gen.internal;

import java.util.LinkedList;
import java.util.List;

public class GeneratorContainer extends GeneratorElement
{
   private List<GeneratorField> generatorFields = new LinkedList<>();
   
   private String name;
   
   private GeneratorPackage pack;

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public GeneratorPackage getPackage()
   {
      return pack;
   }

   public void setPackage(GeneratorPackage pack)
   {
      this.pack = pack;
   }

   public List<GeneratorField> getFields()
   {
      return generatorFields;
   }

   public boolean add(GeneratorField generatorField)
   {
      return generatorFields.add(generatorField);
   }

   public boolean remove(GeneratorField generatorField)
   {
      return generatorFields.remove(generatorField);
   }

}
